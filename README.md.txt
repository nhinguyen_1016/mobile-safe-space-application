Für das Ausführen der Anwendung muss Node.js auf dem Computer vorhanden sein. Falls es das nicht ist, kann es hier 
auf der offiziellen Seite heruntergeladen werden:
https://nodejs.org/en

Die Anwendung wird hier an der obersten Ordnerstruktur(root folder) mit folgendem Befehl ausgeführt: 
npx expo start

Bevor die Anwendung jedoch gestartet werden kann, müssen Sie mit Hilfe des folgenden Befehls alle Abhängigkeiten lokal in Ihrer IDE herunterladen:
npm install
Danach sollte ein node_modules-Ordner erstellt werden. Daraufhin können Sie die Anwendung mit npx expo start starten.

Falls der Befehl nicht funktionieren oder nicht anerkannt werden sollte, sollte der Pfad zu node.js zu der Path-Variable in der Systemvariablen gespeichert werden.
Dazu suchen Sie auf ihrem Computer den Pfad zu ihrem heruntergeladenen Nodejs Ordner und fügen den Pfad der Path Variable in der Systemvariable zu. In meinem Fall
sieht der Pfad so aus:
C:\Program Files\nodejs\ .
Daraufhin können Sie das Starte der Anwendung erneut mit npx expo start versuchen.


Falls Abhängigkeiten nicht gefunden werden sollten, könnten Sie versuchen,  alle fehlenden Abhängigkeiten erneut herunterzuladen. 
Dafür löschen sie die package.json Datei (nicht die package-lock.json Datei!) und laden alle Abhängigkeiten so erneut herunter:
npm install

Bei npx expo Start sollte sich daraufhin das Expo Projekt mitsamt einem QR Code und weiteren Anweisungen in der Konsole öffnen. 
Da sollten Sie in der Lage sein, zwischen dem Development Build und Expo Go zu wechseln (mit der Taste s). Für Entwicklungszwecke können Sie Expo Go benutzen.
Der Development Build ist in diesem Fall dafür da, die Anwendung auf der offiziellen Expo Website zu deployen und zu teilen.

Möchten Sie die Anwendung direkt auf Ihrem Handy testen, laden Sie sich die Expo App in dem App Store/Google Play Store herunter.
Innerhalb der App können Sie den QR Code dann aus der Konsole scannen, sodass sich der Prototyp dort öffnen lässt.
Befindet sich auf Ihrem iOS-Gerät in Expo keine Option zum Scannen oder Kopieren eines Linkes, scannen Sie den QR-Code so mit ihrem Handy ein 
(außerhalb von Expo sollte es mit Apple soweit möglich sein).
Ansonsten müssten Sie sich einen geeigneten Emulator für Ihr Gerät herunterladen.

Der Startpunkt der Anwendung befindet sich in App.js. In dem screens-Ordner befinden sich alle Ansichten der Anwendung. 
Die selbst erstellten Komponenten finden Sie unter components. Diese sind dafür da, in den verschiedenen Ansichten wiederverwendbar zu sein.
Unter data sind alle notwendigen Daten als json-Dateien oder JavaScript-Konstanten enthalten.
Bei assets sind alle verwendeten Bilder vorhanden.
In dem redux-Ordner finden Sie Dateien, die für das Speichern der Datenobjekte, aber auch für das generelle globale State Management in React Native notwendig sind.
actions.js beinhaltet alle Aktionen, die an dem Store durchgeführt werden sollten. Diese Actions sind jedoch keine Funktionen, 
sondern bloße Objekte, die Aktionen benennen.
In reducers.js finden Sie die Reducern und dispatch-Methoden, die den Actions mitteilen, wie genau die Funktionen ausgeführt werden. 
In store.js finden sie eine Ansammlung der Reducern und der Ort, wo der Store persistent in den lokalen Speicher des Gerätes gespeichert wird.

Möchten Sie die Anwendung beenden, drücken Sie Strg + C. 

Zusätzliche Information: Es kann in Expo keine .apk oder .ipa-Datei erstellt werden, wenn die kostenlose Version von EAS(neuer Service für Expo) benutzt wird. 
Außerdem ist ein Apple Developer Account notwendig, um den build-Prozess für iOS-Geräte durchzuführen. Dieser ist kostenpflichtig.
Deswegen konnte für diese Arbeit keine .apk oder .ipa erstellt werden.
Durch einen Umweg wurden jedoch QR-Codes für Android- und Apple-Geräten anstelle der .apk/.ipa-Datei erstellt. Somit lässt sich der Prototyp auch in der Expo App auf dem mobilen Gerät öffnen.
In der obersten Projektstruktur werden Sie Screens für die beiden QR-Codes finden. 

