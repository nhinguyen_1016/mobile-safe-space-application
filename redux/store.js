import {createStore, combineReducers, applyMiddleware} from 'redux';
import thunk from 'redux-thunk'
import {moodReducer, exerciseReducer, badgeReducer, categoryReducer, activityReducer} from './reducers';

import AsyncStorage from '@react-native-async-storage/async-storage';
import {persistReducer} from 'redux-persist'
// import {getPersistConfig} from 'redux-deep-persist'

//Konfigurationsobjekt für Speichern des Stores in den AsyncStorage
const persistConfig = {
    key : 'root',
    // version: 1,
    storage: AsyncStorage,
}

//Zusammenfassen aller Reducer 
const rootReducer = combineReducers({moodReducer, exerciseReducer, badgeReducer, categoryReducer, activityReducer})
 
//Zuweisung an den PersistReducer -> speichert den ganzen Store persistent in den AsyncStorage des Gerätes
const persistedReducer = persistReducer(persistConfig, rootReducer)

// export const store = createStore(rootReducer, applyMiddleware(thunk))
export const store = createStore(persistedReducer, applyMiddleware(thunk))
