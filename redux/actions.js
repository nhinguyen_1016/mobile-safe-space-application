export const SET_MOOD_POINTS = 'SET_MOOD_POINTS'
export const SET_MOOD = 'SET_MOOD'
export const RESET_DAILY_MOOD_POINTS = 'RESET_DAILY_MOOD_POINTS'
export const RESET_DAILY_MOOD = 'RESET_DAILY_MOOD'

export const SET_PHYSICAL_EXERCISE_POINTS = 'SET_PHYSICAL_EXERCISE_POINTS'
export const SET_DONE_PHYSICAL_EXERCISES = 'SET_DONE_PHYSICAL_EXERCISES'

export const SET_MENTAL_EXERCISE_POINTS = 'SET_MENTAL_EXERCISE_POINTS'
export const SET_DONE_MENTAL_EXERCISES = 'SET_DONE_MENTAL_EXERCISES'

export const SET_SOCIAL_EXERCISE_POINTS = 'SET_SOCIAL_EXERCISE_POINTS'
export const SET_DONE_SOCIAL_EXERCISES = 'SET_DONE_SOCIAL_EXERCISES'

export const SET_BADGES = 'SET_BADGES'

export const SET_CATEGORIES = 'SET_CATEGORIES'
export const DELETE_CATEGORIES = 'DELETE_CATEGORIES'
export const SET_CATEGORY_KEY = 'SET_CATEGORY_KEY'


export const SET_ACTIVITIES = 'SET_ACTIVITIES'
export const DELETE_ACTIVITIES = 'DELETE_ACTIVITIES'

export const SET_DONE_ACTIVITIES = 'SET_DONE_ACTIVITIES'
export const RESET_DAILY_DONE_CUSTOM_ACTIVITIES = 'RESET_DAILY_DONE_CUSTOM_ACTIVITIES ' 

export const SET_ACTIVITY_KEY = 'SET_ACTIVITY_KEY'

//alle vorhandene Actions, die Aktionen an den Store definieren
//reducers.js definiert, wie diese Actions durchgeführt werden
//Delete Actions wurden hier nicht umgesetzt!

export const setMoodPoint = moodPoint => dispatch => {
    dispatch({
        type: SET_MOOD_POINTS,
        payload: moodPoint
    });
}

//für Funktionen wie das Zurücksetzen eines Objektes, muss kein payload/Parameter der Funktion übergeben werden
export const resetDailyMoodPoint = () => dispatch => {
    dispatch({
        type: RESET_DAILY_MOOD_POINTS,
    });
}

export const setMood = mood => dispatch => {
    dispatch({
        type: SET_MOOD,
        payload: mood
    });
}

export const resetDailyMood = () => dispatch => {
    dispatch({
        type: RESET_DAILY_MOOD,
    });
}

export const setPhysicalExercisePoint = () => dispatch => {
    dispatch({
        type: SET_PHYSICAL_EXERCISE_POINTS,
    });
}

export const setDonePhysicalExercise = physicalExercise => dispatch => {
    dispatch({
        type: SET_DONE_PHYSICAL_EXERCISES,
        payload: physicalExercise
    });
}


export const setMentalExercisePoint = () => dispatch => {
    dispatch({
        type: SET_MENTAL_EXERCISE_POINTS,
    });
}

export const setDoneMentalExercise = mentalExercise => dispatch => {
    dispatch({
        type: SET_DONE_MENTAL_EXERCISES,
        payload: mentalExercise
    });
}


export const setSocialExercisePoint = () => dispatch => {
    dispatch({
        type: SET_SOCIAL_EXERCISE_POINTS,
    });
}

export const setDoneSocialExercise = socialExercise => dispatch => {
    dispatch({
        type: SET_DONE_SOCIAL_EXERCISES,
        payload: socialExercise
    });
}


export const setCategory = (category, activity) => dispatch => {
    dispatch({
        type: SET_CATEGORIES,
        payload: category, activity
    });    
}

export const deleteCategory = index => dispatch => {
    dispatch({
        type: DELETE_CATEGORIES,
        payload: index
    });
}

export const setCategoryKey = () => dispatch => {
    dispatch({
        type: SET_CATEGORY_KEY,
    })
}

export const setActivityKey = () => dispatch => {
    dispatch({
        type: SET_ACTIVITY_KEY,
        // payload: key
    });
}

export const setActivity = activity => dispatch => {
    dispatch({
        type: SET_ACTIVITIES,
        payload: activity
    });
}

export const deleteActivity = index => dispatch => {
    dispatch({
        type: DELETE_ACTIVITIES,
        payload: index
    });
}

export const setDoneActivity = doneActivity => dispatch => {
    dispatch({
        type: SET_DONE_ACTIVITIES,
        payload: doneActivity
    });
}

export const resetDailyDoneCustomActivities = () => dispatch => {
    dispatch({
        type: RESET_DAILY_DONE_CUSTOM_ACTIVITIES,
    });
}

export const setBadge = badge => dispatch => {
    dispatch({
        type: SET_BADGES,
        payload: badge
    });    
}