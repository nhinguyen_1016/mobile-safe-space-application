import { 

    SET_MOOD_POINTS, 
    RESET_DAILY_MOOD_POINTS,
    SET_MOOD, 
    RESET_DAILY_MOOD,

    SET_PHYSICAL_EXERCISE_POINTS, 
    SET_DONE_PHYSICAL_EXERCISES,

    SET_MENTAL_EXERCISE_POINTS,
    SET_DONE_MENTAL_EXERCISES,

    SET_SOCIAL_EXERCISE_POINTS,
    SET_DONE_SOCIAL_EXERCISES,

    SET_BADGES,

    SET_CATEGORIES,
    DELETE_CATEGORIES,
    SET_CATEGORY_KEY,

    SET_ACTIVITIES,
    DELETE_ACTIVITIES,
    SET_ACTIVITY_KEY,

    SET_DONE_ACTIVITIES,
    RESET_DAILY_DONE_CUSTOM_ACTIVITIES,



} from "./actions";

//Store, der durch den Persistor in Homescreen, auch persistent in dem lokalen Speicher des Endgerätes des Nutzers gespeichert wird

const initialState = {
    categories: [],
    categoryKey: 5,
    activities: [],
    activityKey: 5,
    doneActivities: [],
    moodPoints: 0,
    mood: [],
    badges: [],
    physicalExercisePoints: 0,
    physicalExercises: [],
    mentalExercisePoints: 0,
    mentalExercises: [],
    socialExercisePoints: 0,
    socialExercises: [],
   
  
}

//Reducer für das Eintragen der Stimmung und den Punkten in den Store
//Reducer für das Zurücksetzen der Stimmung, sobald ein neuer Tag anfängt -> aktiviert Widget zum Eintragen der Stimmung erneut
//werden durch die dispatch Methode in den Komponenten aufgerufen und ausgeführt
function moodReducer(state = initialState, action){
    switch(action.type){
        case SET_MOOD_POINTS:
            return {
                ...state, 
                moodPoints : action.payload
            };
        case SET_MOOD:
            return{
                ...state,
                mood: action.payload
            }
        //Zurücksetzen der täglichen Stimmung auf Original Zustand des States -> leeres Array 
        case RESET_DAILY_MOOD:
            return{
                ...state,
                mood: initialState.mood
            }    
        case RESET_DAILY_MOOD_POINTS:
            return{
                ...state,
                moodPoints: initialState.moodPoints
            }
        default: 
            return state
        
    }
}

//Reducer für das Abschließen der vordefinierten Aktivitäten
function exerciseReducer(state = initialState, action){
    switch(action.type){
        case SET_PHYSICAL_EXERCISE_POINTS:
            return{
                ...state,
                physicalExercisePoints: state.physicalExercisePoints + 1 
            };
        case SET_DONE_PHYSICAL_EXERCISES:
            return {
                ...state, 

                //um abgeschlossene Aktivitäten zu dem Array hinzuzufügen,
                // werden die neuen Aktivitäten als Parameter der dispatch Methode mitgegeben (hier der Payload der Funktion)
                physicalExercises: [...state.physicalExercises, action.payload]
            };

        case SET_MENTAL_EXERCISE_POINTS:
            return{
                ...state,
                mentalExercisePoints: state.mentalExercisePoints + 1 
            };
        case SET_DONE_MENTAL_EXERCISES:
            return {
                ...state, 
                mentalExercises: [...state.mentalExercises, action.payload]
            };  
            
        case SET_SOCIAL_EXERCISE_POINTS:
            return{
                ...state,
                socialExercisePoints: state.socialExercisePoints + 1 
            };
        case SET_DONE_SOCIAL_EXERCISES:
            return {
                ...state, 
                socialExercises: [...state.socialExercises, action.payload]
            };   

        default: 
            return state
        
    }
}

//Reducer für das Freischalten von Erfolgen
function badgeReducer(state = initialState, action){
    switch(action.type){
        case SET_BADGES:
            return{
                ...state,
                badges : [...state.badges, action.payload]
        
            }    
            default: 
                return state
            
            
    }

}

//Reducer für das Verwalten von Kategorien
function categoryReducer(state = initialState, action){
    switch(action.type){
        case SET_CATEGORIES:
            return {
                ...state, 
                categories: [...state.categories, action.payload]
            };

        //inkrementierter Index für die selbst erstellten Kategorien, um sie richtig in Homescreen in der Flatlist zu rendern    
        case SET_CATEGORY_KEY:
            return{
                ...state,
                categoryKey: state.categoryKey + 1 
                };
        default: 
            return state
        
    }
}

//Reducer für das Erstellen neuer custom Aktivitäten
function activityReducer(state = initialState, action){
    switch(action.type){
        case SET_ACTIVITIES:
            return {
                ...state, 
                activities: [...state.activities, action.payload]
                // activities: action.payload
            };

        //inkrementierter Index für die selbst erstellten Aktivitäten, um sie richtig in CustomActivitiesScreen in der Flatlist zu rendern    

        case SET_ACTIVITY_KEY:
            return{
                ...state,
                activityKey: state.activityKey + 1 
            };
        case SET_DONE_ACTIVITIES:
            return{
                ...state,
                doneActivities: [...state.doneActivities, action.payload]
            };

        case RESET_DAILY_DONE_CUSTOM_ACTIVITIES:
            return{
                ...state,
                doneActivities: initialState.doneActivities
            };
        default: 
            return state
        
    }
}

export { 
    moodReducer, 
    exerciseReducer,
    badgeReducer,
    categoryReducer,
    activityReducer
}