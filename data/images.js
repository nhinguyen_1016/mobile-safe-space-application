const images = {
    
    //daily theme images
    dailyTheme1Pic: require('../assets/DailyThemeDay1Pic.png'),
    dailyTheme1Rect: require('../assets/DailyThemeDay1Rect.png'),


    //predefined (physical, mental, socia) activity images
    physical1_1Pic: require('../assets/manul3.jpg'),
    physical1_2Pic: require('../assets/manul2.jpg'),

    //unlocked badge images
    unlocked_badge_MH_1: require('../assets/badges/unlocked-badge-MH-1.png'),
    unlocked_badge_MH_2: require('../assets/badges/unlocked-badge-MH-2.png'),
    unlocked_badge_MH_3: require('../assets/badges/unlocked-badge-MH-3.png'),
    unlocked_badge_MH_4: require('../assets/badges/unlocked-badge-MH-4.png'),

    unlocked_badge_MH2_1: require('../assets/badges/unlocked-badge-MH2-1.png'),

    unlocked_badge_PH_1: require('../assets/badges/unlocked-badge-PH-1.png'),
    unlocked_badge_PH_2: require('../assets/badges/unlocked-badge-PH-2.png'),

}

export default images;