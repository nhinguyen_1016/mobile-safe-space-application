const asyncStorageKeys = {

    // date data
    storedDate : 'storedDate',

    //day 1 data
    physicalExercises_day1_storage : 'physicalExercises_day1',
    physicalExercisePoints_day1_storage : 'physicalExercisePoints_day1',

    mentalExercises_day1_storage : 'mentalExercises_day1',
    mentalExercisePoints_day1_storage : 'mentalExercisePoints_day1',

    socialExercises_day1_storage : 'socialExercises_day1',
    socialExercisePoints_day1_storage : 'socialExercisePoints_day1',

    customExercises_day1_storage :  'customExercises_day1',

    mood_day1_storage : 'mood_day1',
    moodPoints_day1_storage : 'moodPoints_day1',


    //day 2 data
    physicalExercises_day2_storage : 'physicalExercises_day2',
    physicalExercisePoints_day2_storage : 'physicalExercisePoints_day2',

    mentalExercises_day2_storage : 'mentalExercises_day2',
    mentalExercisePoints_day2_storage : 'mentalExercisePoints_day2',

    socialExercises_day2_storage : 'socialExercises_day2',
    socialExercisePoints_day2_storage : 'socialExercisePoints_day2',

    customExercises_day2_storage :  'customExercises_day2',

    mood_day2_storage : 'mood_day2',
    moodPoints_day2_storage : 'moodPoints_day2',


    //day 3 data
    physicalExercises_day3_storage : 'physicalExercises_day3',
    physicalExercisePoints_day3_storage : 'physicalExercisePoints_day3',

    mentalExercises_day3_storage : 'mentalExercises_day3',
    mentalExercisePoints_day3_storage : 'mentalExercisePoints_day3',

    socialExercises_day3_storage : 'socialExercises_day3',
    socialExercisePoints_day3_storage : 'socialExercisePoints_day3',

    customExercises_day3_storage :  'customExercises_day3',

    mood_day3_storage : 'mood_day3',
    moodPoints_day3_storage : 'moodPoints_day3',

    //day 4 data
    physicalExercises_day4_storage : 'physicalExercises_day4',
    physicalExercisePoints_day4_storage : 'physicalExercisePoints_day4',

    mentalExercises_day4_storage : 'mentalExercises_day4',
    mentalExercisePoints_day4_storage : 'mentalExercisePoints_day4',

    socialExercises_day4_storage : 'socialExercises_day4',
    socialExercisePoints_day4_storage : 'socialExercisePoints_day4',

    customExercises_day4_storage :  'customExercises_day4',

    mood_day4_storage : 'mood_day4',
    moodPoints_day4_storage : 'moodPoints_day4',


    //day 5 data
    physicalExercises_day5_storage : 'physicalExercises_day5',
    physicalExercisePoints_day5_storage : 'physicalExercisePoints_day5',

    mentalExercises_day5_storage : 'mentalExercises_day5',
    mentalExercisePoints_day5_storage : 'mentalExercisePoints_day5',

    socialExercises_day5_storage : 'socialExercises_day5',
    socialExercisePoints_day5_storage : 'socialExercisePoints_day5',

    customExercises_day5_storage :  'customExercises_day5',

    mood_day5_storage : 'mood_day5',
    moodPoints_day5_storage : 'moodPoints_day5',


    //day 6 data
    physicalExercises_day6_storage : 'physicalExercises_day6',
    physicalExercisePoints_day6_storage : 'physicalExercisePoints_day6',

    mentalExercises_day6_storage : 'mentalExercises_day6',
    mentalExercisePoints_day6_storage : 'mentalExercisePoints_day6',

    socialExercises_day6_storage : 'socialExercises_day6',
    socialExercisePoints_day6_storage : 'socialExercisePoints_day6',

    customExercises_day6_storage :  'customExercises_day6',

    mood_day6_storage : 'mood_day6',
    moodPoints_day6_storage : 'moodPoints_day6',
    
}

export default asyncStorageKeys;