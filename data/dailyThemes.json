{

    "dailyTheme1" : {
        "id": "dailyTheme1",
        "title": "Das Verarbeiten und Heilen deines Traumas",
        "content": "Du selbst kannst einiges tun, um die Heilung deines Traumas zu unterstützen. Das Wichtigste ist, dass du dich dabei nicht unter Druck setzt. \n\nEs ist außerdem wichtig wahrzunehmen, dass du vollkommen unschuldig warst und du nichts dafür konntest.  \n\nUm ein Trauma zu heilen, braucht es viel Zeit und einen liebevollen und geduldigen Umgang mit dir selbst und deinen Gefühlen. Dein Trauma entspringt dem Verlust deines Sicherheitsgefühls und der Überreizung deines Nervensystems. \nDeshalb trägt alles, was deine innere Ruhe und Sicherheit fördert, zur Linderung der Traumafolgen bei. Und auch das bewusste Verbinden mit deinem Körper und deinem Geist z.B. durch achtsame Selbstfürsorge kann hilfreich sein, denn viele traumatisierte Menschen haben das Vertrauen in den eigenen Körper verloren, weil sie dessen Reaktionen im Rahmen des Traumas und dessen Folgen nicht bewusst steuern konnten. \n\nDadurch haben sich unter Umständen auch negative Glaubenssätze manifestiert, die es aufzudecken und zu lösen gilt, um die Beziehung zu deinem Körper und auch deiner Psyche zu stabilisieren.",
        "fullImage": "../assets/DailyThemeDay1Pic.png",
        "imagePreview": "../assets/picture-rec.png"
    },

    "dailyTheme2" : {
        "id": "dailyTheme2",
        "title": "Glaubenssätze",
        "content": "Glaubenssätze sind Überzeugungen, die tief in unserem Unterbewusstsein verankert sind. Wir halten sie für wahr und nehmen durch sie die Welt um uns herum ebenso wie uns selbst wahr. Sie sind also wie ein individueller Filter, durch den wir uns sehen und bewerten. \nWährend uns positive Glaubenssätze zwar innewohnen und uns durchaus bestärken können, sind die negativen Gedanken doch immer wieder die, die es an die Oberfläche schaffen und unsere Selbstwahrnehmung von dort aus manipulieren. \n\nUm solch negative Glaubenssätze erfolgreich auflösen zu können, musst du sie natürlich erst einmal identifizieren. Fällt dir auf, dass dir im Alltag immer wieder die gleichen Situationen begegnen? \n Es ist nicht einfach, eigenen Glaubenssätzen auf die Schliche zu kommen. Man muss sich ganz genau beobachten und wird nicht von heute auf morgen alle dieser vermeintlichen Wahrheiten als falsch entschlüsseln. Denn meist ist es nicht nur ein limitierender Glaubenssatz, der dich begleitet. Oftmals gibt es einen dominierenden Glaubenssatz, der öfter in bestimmten Situationen getriggert wird, als andere Glaubenssätze. \n\nMehr noch, du kannst negative Glaubenssätze im nächsten Schritt ändern, indem du ihnen etwas Positives entgegensetzt: »Ich bin mehr als genug« oder »Ich bin gut so, wie ich bin«. \n\nEs kann schmerzhaft sein, sich diesen verdrängten schwierigen Emotionen wieder anzunähern und sie gemeinsam mit den limitierenden Glaubenssätzen aufzulösen. Was nach viel Arbeit klingt, gibt dir aber auch die Möglichkeit, dich besser kennen zu lernen und mehr für deine Bedürfnisse da zu sein. ",
        "fullImage": "../assets/DailyThemeDay1Pic.png",
        "imagePreview": "../assets/picture-rec.png"
    },

    "dailyTheme3" : {
        "id": "dailyTheme3",
        "title": "Verwundbarkeit",
        "content": "Obwohl sowohl körperliche als auch seelische Verletzlichkeit zu unserer Natur gehört, versuchen wir gerne, diese scheinbare Schwäche zu verbergen. Der Grund: Niemand von uns möchte verletzt werden. Schmerzen sind ja auch etwas Ungutes. \n\nAber genau diese Strategie ist häufig dafür verantwortlich, dass wir uns machtlos, ausgeliefert oder einfach leer fühlen. Während uns die Vermeidung Energie nimmt, kann Authentizität und „Selbstehrlichkeit“ Energie geben. \n\n Sich verletzlich zu zeigen ist ein Risiko. Aber wenn wir dieses Risiko nicht eingehen, dann bleiben uns viele wunderbare Chancen im Leben verwehrt. Derjenige, der sich einer Situation aussetzt, in der er verletzt werden könnte. Beispielsweise, wenn er einen Menschen seine Liebe gesteht oder wenn er anderen gegenüber eingesteht, dass er Hilfe braucht. Genau deshalb ist Verletzlichkeit eine Stärke, die uns manchmal zwar voller Wucht auf den Boden knallt, aber uns oftmals auch die glücklichsten Momente unseres Lebens beschert. \n\nVerletzlichkeit bedeutet auch, seiner Kränkung Ausdruck zu verleihen. Den Schmerz also nicht in sich hineinzufressen. Durch das Ausleben der Gefühle kann der Schmerz entweichen und innere Blockaden können sich lösen.",
        "fullImage": "../assets/DailyThemeDay1Pic.png",
        "imagePreview": "../assets/picture-rec.png"
    },

    "dailyTheme4" : {
        "id": "dailyTheme4",
        "title": "Das Ausdrücken eigener Bedürfnisse",
        "content": "Wir alle kennen einerseits positive Gefühle wie Zufriedenheit oder Freude aber andererseits auch negative Gefühle wie zum Beispiel Traurigkeit, Wut, oder Frustration. Wenn du spürst, dass sich irgendetwas in dir nicht gut und ausgeglichen anfühlt, solltest du diesen Gefühlen auf den Grund gehen und herausfinden, was es genau ist. Bist du frustrierst, verärgert oder traurig? \n\nVersuche, die Bedürfnisse hinter deinen Gefühlen zu erkennen, auch wenn es am Anfang nicht einfach ist. Dafür ist es wiederum hilfreich, dir einen Moment Zeit zu nehmen.Frage dich selbst, was du in diesem Moment bräuchtest, um dich gut zu fühlen. Alternativ kannst du dich an Situationen erinnern, in denen du dich sehr wohl gefühlt hast. Überlege dann, welche Verhaltensweisen oder Aktivitäten zu diesem Wohlfühlen beigetragen haben. \n\nDie eigenen Bedürfnisse einerseits richtig zu erkennen und dann auch noch richtig zu verbalisieren ist zwar nicht immer leicht, den Aufwand aber allemal wert – denn durch die richtige Kommunikation können wir in unseren Beziehungen mehr Harmonie, Zufriedenheit Vertrauen erreichen. \n\nWenn du den ersten Schritt machst und dein Bedürfnis wahrnimmst und richtig kommunizierst, d. h. jemand, der dir nahe steht die Gefühle und Hintergründe des Bedürfnisses erklärst, wird die Person im Normalfall gleichtun – dadurch kannst du die Basis für ein offenes Gespräch schaffen, das die Voraussetzung für die Lösung deines Problems oder die Erfüllung deines Wunsches ist. \n\n" ,
        "fullImage": "../assets/DailyThemeDay1Pic.png",
        "imagePreview": "../assets/picture-rec.png"
    },

    "dailyTheme5" : {
        "id": "dailyTheme5",
        "title": "Sorge dich genug um dich",
        "content": "Selbstfürsorge sollte nichts sein, dass du dir „verdienen“ musst, weil du besonders ausgelaugt bist oder viel geleistet hast. Einen fürsorgenden Blick auf dich selbst zu haben, sollte vielmehr ein natürlicher und selbstverständlicher Teil von dir sein. Es ist eine Haltung, eine Einstellung, dir selbst gegenüber, die sich in deinem Alltag in ganz vielen kleinen Handlungen widerspiegelt. Sich um sich selbst zu kümmern bedeutet, dass du bewusst die Entscheidung triffst, Ja   zu dir und deinen Bedürfnissen zu sagen. \n\n\nEntwickle eine innere Haltung, die Mitgefühl und Wertschätzung gegenüber dir selbst ausdrückt. Wenn du dich wertschätzt, dann gehst du auch automatisch gut mit dir um. Dann nimmst du deine eigenen Bedürfnisse, Gefühle und Ressourcen ernst und arbeitest automatisch an deinem Wunsch, körperlich, seelisch und geistig gesund zu bleiben. Selbstfürsorge ist dabei nicht immer leicht, ganz im Gegenteil. Trotzdem bedeutet für sich selbst zu sorgen, genauer hinzuschauen. Und dich wirklich mit dir auseinanderzusetzen. \n\nWenn du das tust, wirst du eventuell feststellen, dass sich unbemerkt einige Denk- und Verhaltensweisen bei dir eingeschlichen haben, die regelrechte Energieräuber sind. Die dir in Wahrheit nicht gut tun und von denen du dich eigentlich trennen solltest. Und auch das bedeutet Selbstfürsorge: Loslassen, Grenzen kommunizieren und Nein zu sagen, deinen negativen Gefühlen Raum geben.",
        "fullImage": "../assets/DailyThemeDay1Pic.png",
        "imagePreview": "../assets/picture-rec.png"
    },

    "dailyTheme6" : {
        "id": "dailyTheme6",
        "title": "Day 6 Daily Theme Title",
        "content": "Day 6 Daily Theme Content",
        "fullImage": "../assets/DailyThemeDay1Pic.png",
        "imagePreview": "../assets/picture-rec.png"
    }

}