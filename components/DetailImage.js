import React, { useState } from 'react';
import { Image, View } from 'react-native';
import { StyleSheet } from 'react-native';
import images from '../data/images';

function DetailImage({type}) {

    var image = images.dailyTheme1Pic

    return (

        //soll Bild nicht bei einer Custom Activity rendern
        <View>
                {type == 'customActivity' || type == 'customActivity2' || type == 'CustomActivity2' ? 
                    <View/> :
                    <Image
                        style={styles.image}
                        resizeMode="cover"
                        source={image}
                    />}
        </View>
      
    );
    
}

const styles = StyleSheet.create({
    image: {
        width: 360,
        height: 250,
            
    },

})

export default DetailImage;