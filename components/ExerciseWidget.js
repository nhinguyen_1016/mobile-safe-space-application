import React from 'react';
import { View, Text, Pressable } from 'react-native';
import { useNavigation } from '@react-navigation/native';

import { StyleSheet } from 'react-native';

//Parameter bekommen durch Physical/Mental/SocialExerciseScreen und CustomCategoryScreen, um passende Aktivität als Widget zu rendern
function ExerciseWidget({ name, stackName }) {

    const navigation = useNavigation();

    return (
        <Pressable style={styles.dailyExercisesView} onPress={() => navigation.navigate(stackName, {
            title: name
        })}>
            <Text style={styles.normalText}>
                {name}
            </Text>
        </Pressable>
    );
}

const styles = StyleSheet.create({
    dailyExercisesView: {
        backgroundColor: 'white',
        borderRadius: 10,
        width: 300,
        height: 100,
        justifyContent: "center",
        alignItems: "center",
        margin: 10,
        shadowColor: 'black',
        shadowOpacity: 1,
        elevation: 4,
        shadowRadius: 4,
        shadowOffset: {
            width: 0,
            height: 4
        },
    },

    normalText: {
        fontSize: 20,
    },

})

export default ExerciseWidget;