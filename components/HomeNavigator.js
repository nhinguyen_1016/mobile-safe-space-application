import { createStackNavigator } from '@react-navigation/stack'

import LoadingScreen from '../screens/LoadingScreen';
import WelcomeScreen from '../screens/WelcomeScreen';
import Homescreen from '../screens/Homescreen';
import DailyThemeScreen from '../screens/DailyThemeScreen';
import PhysicalExerciseScreen from '../screens/PhysicalExerciseScreen'
import MentalExerciseScreen from '../screens/MentalExerciseScreen'
import SocialExerciseScreen from '../screens/SocialExerciseScreen'
import ExerciseDetailScreen from '../screens/ExerciseDetailScreen';
import CreateCategoryScreen from '../screens/CreateCategoryScreen';
import CreateActivityScreen from '../screens/CreateActivityScreen';
import EditCustomActivityScreen from '../screens/EditCustomActivityScreen';
import CustomActivitiesScreen from '../screens/CustomActivitiesScreen';
import { Button } from 'react-native';

const Stack = createStackNavigator()

const HomeNavigator = () => {
    return (
  
      <Stack.Navigator>
  
        <Stack.Screen
          name='loading_screen'
          component={LoadingScreen}
          options={{ headerShown: false }}
        />
  
        <Stack.Screen
          name='home_screen'
          component={Homescreen}
          options={{ title: "Heute, " + new Date().toLocaleDateString() }}
        />
  
        <Stack.Screen
          name='daily_theme_screen'
          component={DailyThemeScreen}
          options={{ title: "Heutiges Thema" }}
        />
  
        <Stack.Screen
          name='physical_screen'
          component={PhysicalExerciseScreen}
          options={{ title: "Physische Aktivitäten" }}
        />
  
        <Stack.Screen
          name='mental_screen'
          component={MentalExerciseScreen}
          options={{ title: "Mentale Aktivitäten" }}
  
        />
  
        <Stack.Screen
          name='social_screen'
          component={SocialExerciseScreen}
          options={{ title: "Soziale Aktivitäten" }}
  
        />
  
        <Stack.Screen
          name='exercise_detail_screen'
          component={ExerciseDetailScreen}
          options={{ title: "" }}  
        />

        <Stack.Screen
          name='create_category_screen'
          component={CreateCategoryScreen}
          options={{ title: "Eigene Kategorie" }}
        />

        <Stack.Screen
          name='create_activity_screen'
          component={CreateActivityScreen}
          options={{ title: "Eigene Aktivität" }}
        />

        <Stack.Screen
          name='edit_custom_activity_screen'
          component={EditCustomActivityScreen}
          options={{ title: "Editieren" }}
        />

        <Stack.Screen
          name='category_detail_screen'
          component={CustomActivitiesScreen}
          options={{ title: "" }}
        />

      </Stack.Navigator>
    )
  }
  
  export default HomeNavigator;