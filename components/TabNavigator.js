import { NavigationContainer } from '@react-navigation/native';
import { createBottomTabNavigator } from '@react-navigation/bottom-tabs';
import { getFocusedRouteNameFromRoute } from '@react-navigation/native';

import PersonalSpaceScreen from '../screens/PersonalSpaceScreen'
import BadgesScreen from '../screens/BadgesScreen'
import ProgressScreen from '../screens/ProgressScreen'

import HomeNavigator from './HomeNavigator';
import PersonalSpaceNavigator from './PersonalSpaceNavigator';

import FontAwesome5 from 'react-native-vector-icons/FontAwesome5'

const Tab = createBottomTabNavigator()


function TabNavigator() {

  const getTabBarVisibility = (route) => {
    const routeName = getFocusedRouteNameFromRoute(route)
    const hideOnScreens = ['daily_theme_screen', 'physical_screen']
    return hideOnScreens.indexOf(routeName) <= -1;
  };

  return (

    <NavigationContainer>
      <Tab.Navigator
        screenOptions={({ route }) => ({

          tabBarActiveTintColor: '#E28282',
          tabBarInactiveTintColor: '#808080',
          tabBarVisible: getTabBarVisibility(route),
          tabBarHideOnKeyboard: true,

          tabBarIcon: ({ focused, size, color }) => {
            let iconName;

            if (route.name === 'Home') {
              iconName = 'house-user'

            } else if (route.name === 'Ich') {
              iconName = 'user'

            } else if (route.name === 'badges_screen') {
              iconName = 'certificate'

            } else if (route.name === 'progress_screen') {
              iconName = 'circle'
            }

            return (
              <FontAwesome5
                name={iconName}
                size={size}
                color={color}
              />
            )

          }
        })}

      >

        <Tab.Screen
          name='Home'
          component={HomeNavigator}
          options={{ headerShown: false }}
        />

        <Tab.Screen
          name='Ich'
          component={PersonalSpaceNavigator}
          options={{ headerShown: false }}

        />

        <Tab.Screen
          name='badges_screen'
          component={BadgesScreen}
          options={{ title: "Erfolge" }}

        />

        <Tab.Screen
          name='progress_screen'
          component={ProgressScreen}
          options={{ title: "Fortschritt" }}

        />

      </Tab.Navigator>
    </NavigationContainer>

  );
}

export default TabNavigator;

