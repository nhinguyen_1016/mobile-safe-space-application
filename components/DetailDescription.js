import React, { useEffect, useRef, useState } from 'react';
import { View, Text, Image, TouchableOpacity, Pressable, SafeAreaView, StatusBar, Platform, FlatList, ImageBackground, Dimensions, TextInput } from 'react-native';

import { useNavigation } from '@react-navigation/native';

import { StyleSheet } from 'react-native';

import {useSelector, useDispatch} from 'react-redux'
import { setActivity, setActivityKey } from '../redux/actions';
import {store} from '../redux/store';


//Parameter bekommen durch folgende Komponenten:
//DailyThemeScreen, ExerciseDetailScreen, CreateActivityScreen
function DetailDescription({title, description, editable, index, type, category}){

    //benötigt für das Erstellen custom Aktivitäten
    const [customTitle, setCustomTitle] = useState("")
    const [customContent, setCustomContent] = useState("")
    
    const navigation = useNavigation();
    const dispatch = useDispatch()


    const handleOnSave = () => {

        //Erstellen einer neuen Aktivität -> Unterscheidung, von welchem Screen das Erstellen ausging
        if(type == 'customActivity'){
            //zurück zu CreateCategoryScreen
            console.log("DetailDescription: created new activity from create-category screen")
            navigation.navigate('create_category_screen', {
                title: customTitle,
                description: customContent,
                editable: false
            })

        }else{
            console.log("DetailDescription: created new activity from category-detail screen")

            //create new activity and dispatch to store before going to detail view of new activity
            const newActivity = {
                key: store.getState().activityReducer.activityKey, index: store.getState().activityReducer.activityKey, category: category, title: customTitle, id: "CustomActivity_" + customTitle , description: customContent, done: false
            }

            console.log("DetailDescription: handleOnSave creating new activity2: " + JSON.stringify(newActivity))
            
            dispatch(setActivityKey())
            dispatch(setActivity(newActivity))
            console.log("DetailDescription: handleOnSave() Store state new activities: " + JSON.stringify(store.getState().activityReducer.activities))

            navigation.navigate('exercise_detail_screen', {
                title: customTitle,
                description: customContent,
                id: 'CustomActivity_' + customTitle,
                backToActivityScreen: true
               
            })
        }
    }

    //Editierfunktion einer Aktivität -> nicht umgesetzt
    // const handleOnSaveEdit = () => {
    //     navigation.navigate('create_category_screen', {
    //         title: customTitle,
    //         description: customContent,
    //         editable: true,
    //         index: index
    //     })
    // }

    useEffect(() => {
        if(type == 'customActivity' || type == 'customActivity2' || type == 'CustomActivity2'){
            console.log("DetailDescription: CUSTOM ACTIVITY. ")
        }else{
            console.log("DetailDescription: NOT A CUSTOM ACTIVITY.")
        }
    
        if(editable){
            console.log("DetailDescription: EDITING")  

            setCustomTitle(title) 
            setCustomContent(description)

        }else{
            console.log("DetailDescription: CREATING or VIEWING")
        }
    }, [])

       return(
        <View style={styles.whiteRec}>
            {/* beim Erstellen einer Aktivität -> TextInput für den User */}
            {type == 'customActivity' || type == 'CustomActivity2' ? 
                <TextInput
                    style={styles.customTitle}
                    placeholder='Titel'
                    maxLength={40}
                    defaultValue={title}
                    editable={true}
                    onChangeText={(value) => {setCustomTitle(value)}}
                /> :  
                // ansonsten kein TextInput, z.B für eine Detailansicht zum Lesen
                <Text style={styles.title} >
                    {title}
                </Text>
            }

            <Image
                style={styles.seperator}
                contentFit="cover"
                source={require('../assets/trennlinie.png')}
            />

            {type == 'customActivity' || type == 'CustomActivity2' ?  
                <TextInput
                    style={styles.customContent}
                    placeholder='Beschreibung'
                    multiline={true}
                    defaultValue={description}
                    editable={true}
                    onChangeText={(value) => {setCustomContent(value)}}
                /> :  
                <Text style={styles.contentText} >
                    {description}
                </Text>
            }

            {type == 'customActivity' || type == 'CustomActivity2' ? 
                <TouchableOpacity 
                    style={[styles.saveButton, !customTitle  ? {backgroundColor: '#d3d3d3'} : {backgroundColor: '#e28282'} ]} 
                    disabled={!customTitle}
                    onPress={handleOnSave}
                    // onPress={editable ? handleOnSaveEdit : handleOnSave}
                >
                    <Text style={[styles.buttonText, {color: 'white', fontWeight: 'bold'}]} >
                        Speichern
                    </Text>   
                </TouchableOpacity> 
                : <View/>
            }

        </View>
    );
}

const styles = StyleSheet.create({
    whiteRec: {
        justifyContent: 'flex-start',
        alignItems: 'center',
        gap: 30,
        // width: 365,
        width: Dimensions.get('window').width,
        height: Dimensions.get('window').height + 260,
        backgroundColor: 'white',
        paddingLeft: '10%',
        paddingRight: '10%',        
    },

    title: {
        fontSize: 20,
        marginTop: '8%'
    },

    seperator: {
        width: '100%',
        height: 1,
        backgroundColor: '#808080',
        
    },

    contentText: {
        fontSize: 16,
    },

    customTitle: {
        marginTop: 30,
        paddingLeft: 20,
        justifyContent: 'center',
        alignItems: 'center',
        width: 300,
        height: 40,
        borderRadius: 5,
        fontSize: 16,
        shadowColor: 'black',
        shadowOpacity: 1,
        elevation: 4,
        shadowRadius: 4,
        shadowOffset: {
            width: 0,
            height: 4
        },
    },

    customContent: {
        justifyContent: 'flex-start',
        alignItems: 'flex-start',
        flexDirection: 'row',
        flexWrap: 'wrap',
        alignContent: 'flex-start',
        width: 300,
        height: '30%',
        paddingLeft: '10%',
        paddingRight: '10%',
        borderRadius: 5,
        fontSize: 16,
        shadowColor: 'black',
        shadowOpacity: 1,
        elevation: 4,
        shadowRadius: 4,
        shadowOffset: {
            width: 0,
            height: 4
        },
    },

    saveButton: {
        borderRadius: 5,
        width: 250,
        height: 50,
        justifyContent: 'center',
        alignItems: 'center',        
    },

    buttonText: {
        fontSize: 16,
        color: 'black',
    },

})

export default DetailDescription;