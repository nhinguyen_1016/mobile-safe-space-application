import React, { useState } from 'react';
import { View, Text, Pressable, Button, TouchableOpacity } from 'react-native';
import { useNavigation } from '@react-navigation/native';

import { StyleSheet } from 'react-native';

//aufgerufen durch DailyFeelingsScreen, um jede Emotion als Button zu rendern
function FeelingsButton({ id, emotion, points, onPress}) {

    const[isPressed, setIsPressed] = useState(false)

    const handleOnButtonPress = () => {
        if(!isPressed){
            setIsPressed(true)
            onPress(isPressed)
        } else{
            setIsPressed(false)
            onPress(isPressed)
        }
    }

    return (
        <TouchableOpacity
            style={[styles.emotionButton, isPressed ?  {backgroundColor: '#ffd8b5'} :  {backgroundColor: 'white'} ]} 
            onPress={handleOnButtonPress}
        >
            <Text style={styles.buttonText}>
                {emotion}
            </Text>
        </TouchableOpacity>
    );
}

const styles = StyleSheet.create({

    emotionButton: {
        borderRadius: 5,
        width: 100,
        height: 35,
        justifyContent: "center",
        alignItems: "center",
    },

    buttonText: {
        fontSize: 16,

    }

})

export default FeelingsButton;