import { createStackNavigator } from '@react-navigation/stack'

import PersonalSpaceScreen from '../screens/PersonalSpaceScreen'
import DailyFeelingsScreen from '../screens/DailyFeelingsScreen'

const Stack = createStackNavigator()

const PersonalSpaceNavigator = () => {
    return (
  
      <Stack.Navigator>
  
        <Stack.Screen
          name='personal_space_screen'
          component={PersonalSpaceScreen}
          options={{ title: "Mein persönlicher Bereich" }}
        />
  
        <Stack.Screen
          name='daily_feelings_screen'
          component={DailyFeelingsScreen}
          options={{ title: "" }}
        
        />

      </Stack.Navigator>
    )
  }
  
  export default PersonalSpaceNavigator;