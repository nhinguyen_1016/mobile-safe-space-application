import React, { useEffect, useState, useRef } from 'react';
import { View, Text, Pressable, Button, TouchableOpacity } from 'react-native';
import { useNavigation } from '@react-navigation/native';

import {store} from '../redux/store';
import { useSelector, useDispatch } from 'react-redux';

import { StyleSheet } from 'react-native';
import { setPhysicalExercisePoint, setDonePhysicalExercise, setActivity } from '../redux/actions';
import { setMentalExercisePoint, setDoneMentalExercise } from '../redux/actions';
import { setSocialExercisePoint, setDoneSocialExercise } from '../redux/actions';
import { setDoneActivity } from '../redux/actions';

import * as Notifications from 'expo-notifications'
import NotificationPopup from 'react-native-push-notification-popup'

Notifications.setNotificationHandler({
  handleNotification: async () => ({
    shouldShowAlert: true,
    shouldPlaySound: false,
    shouldSetBadge: false,
  })
})

//Parameter bekommen durch Physical/Mental/SocialExerciseScreens und CustomActivityScreens
function ExerciseDetailWidget({index, id, title, description, stackName, done, category}) {


    const notificationData = require('../data/localNotifications.json');
    const [notification, setNotification] = useState(false)
    const notificationListener = useRef()
    const responseListener = useRef()
    // var popup;

    const navigation = useNavigation();
    const dispatch = useDispatch()

    const customActivities = useSelector(state => state.activityReducer.activities)
    const storeBadges = store.getState().badgeReducer.badges
    const storePhysical = useSelector(state => state.exerciseReducer.physicalExercises)
    const [exerciseStorageData, setExerciseStorageData] = useState('')

        const handleOnPress = () => {
        let newActivity

        //hier wird zwischen vordefinierten und custom Activities unterschieden
        //Grund: bei Custom Activity -> zu Beginn eines neuen Tages soll custom Activity wieder abschließbar sein und dafür eine done-property bekommen
        if(!id.includes('CustomActivity_')){
            console.log("ExerciseDetailWidget: handleOnPress() \n NOT A CUSTOM ACTIVITY")

            newActivity = {
                index: index, title: title, description: description, done: true, id: id
            }
            console.log("ExerciseDetailWidget: Following activity done: " + JSON.stringify(newActivity))


        }else{
            console.log("ExerciseDetailWidget: handleOnPress() \n CUSTOM ACTIVITY")
            newActivity = {
                index: index, title: title, description: description, done: true, id: id, category: category, key: index
            }

            console.log("ExerciseDetailWidget: Following activity done: " + JSON.stringify(newActivity))

        }

        //unterschieden zwischen Typen der Aktivität -> wichtig für das Freischalten bestimmter Erfolge && Darstellung in der Timeline
        if(newActivity.id.includes('physical')) {
            dispatch(setPhysicalExercisePoint())
            dispatch(setDonePhysicalExercise(newActivity))

            let badge = storeBadges.find((badge) => badge.key == '4')
            if(!badge && store.getState().exerciseReducer.physicalExercisePoints == 2){
                console.log("ExerciseDetailWidget: Physical badge LV.1 doesn't exist yet! Send notification soon .")
                schedulePushNotification(store.getState().exerciseReducer.physicalExercisePoints)

            }else{
                console.log("ExerciseDetailWidget: Physical badge LV.1 already exists!")
            }

            let badge2 = storeBadges.find((badge2) => badge2.key == '5')
            if(!badge2 && store.getState().exerciseReducer.physicalExercisePoints == 4){
                console.log("ExerciseDetailWidget: Physical badge LV.12 doesn't exist yet! Send notification soon .")
                schedulePushNotification(store.getState().exerciseReducer.physicalExercisePoints)

            }else{
                console.log("ExerciseDetailWidget: Physical badge LV.2 already exists!")
            }
            

        }

        if(newActivity.id.includes('mental')) {
            dispatch(setMentalExercisePoint())
            dispatch(setDoneMentalExercise(newActivity))

        }

        if(newActivity.id.includes('social')) {
            dispatch(setSocialExercisePoint())
            dispatch(setDoneSocialExercise(newActivity))

        }

        if (newActivity.id.includes('CustomActivity_')){
            dispatch(setDoneActivity(newActivity))
            console.log("ExerciseDetailWidget: handleOnPress() \n Updated store with finished activities \n " + JSON.stringify(store.getState().activityReducer.doneActivities))
        }
    
    }

    const schedulePushNotification = async (points) =>  {
        let notificationTitle
        let notificationBody

        if(points == 2){
            notificationTitle = notificationData.PH_1_badge.title
            notificationBody = notificationData.PH_1_badge.body

        }else if(points == 4){
            notificationTitle = notificationData.PH_2_badge.title
            notificationBody = notificationData.PH_2_badge.body
        }

        await Notifications.scheduleNotificationAsync({
          content: {
            title: notificationTitle,
            body: notificationBody,
          },
          trigger: {seconds: 1},
        })
    
        popup.show({
          onPress: function() {
            navigation.navigate('badges_screen')
          },
          appTitle: 'Safe Space Anwendung',
          timeText: '',
          title: notificationTitle,
          body: notificationBody,
          slideOutTime: 3000,
        })
      }

      const renderCustomPopup = ({ appIconSource, appTitle, timeText, title, body }) => (
        <View style= {styles.localNotification}>
            <Text style={styles.localNotificationText}>
                {title}
            </Text>
            <Text style={[styles.localNotificationText, {fontWeight: 'normal'}]}>
                {body}
            </Text>
        </View>
    );

    useEffect(() => {

        notificationListener.current = Notifications.addNotificationReceivedListener(notification => {
            console.log("NOTIFICAITON RECEIVED")
            setNotification(notification)
        })
                
        responseListener.current = Notifications.addNotificationResponseReceivedListener(response => {
            console.log(response)
            navigation.navigate('badges_screen')
        })

        return () => {
            Notifications.removeNotificationSubscription(notificationListener.current)
            Notifications.removeNotificationSubscription(responseListener.current)
        }

    }, [])

    useEffect(() => {

    },[storePhysical])

    return (

        <View style={styles.container} >
            <Pressable 
                style={styles.dailyExercisesView} 
                onPress={() => navigation.navigate(stackName, {
                    id: id,
                    title: title,
                    description: description,
                })}
            >
                <Text style={styles.normalText}>
                    {title}
                </Text>
            </Pressable>

            <TouchableOpacity 
                style={[styles.finishedButton, done ? { backgroundColor: '#d3d3d3'} : {backgroundColor : 'white'} ]} 
                //Aktivität abgeschlossen -> Deaktivieren des Buttons
                onPress={done ? null : handleOnPress}
            >
                <Text style={[styles.buttonText , done ? {color: 'white'} : {color: 'black'}]} >
                    Abgeschlossen
                </Text>   
            </TouchableOpacity>
        
            <NotificationPopup
                ref={ref => popup = ref}
                renderPopupContent={renderCustomPopup}
                shouldChildHandleResponderStart={true}
                shouldChildHandleResponderMove={true}
                isSkipStatusBarPadding={true}
            />
                
        </View>
    );

}

const styles = StyleSheet.create({

    container: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
        gap: 10
    },

    dailyExercisesView: {
        backgroundColor: 'white',
        borderTopLeftRadius: 10,
        borderTopRightRadius: 10,
        width: 300,
        height: 100,
        justifyContent: "center",
        alignItems: "center",
        shadowColor: 'black',
        shadowOpacity: 1,
        elevation: 4,
        shadowRadius: 4,
        shadowOffset: {
            width: 0,
            height: 4
        },
    },

    normalText: {
        fontSize: 20,
    },

    finishedButton: {
        borderRadius: 5,
        // backgroundColor: 'white',
        width: 300,
        height: 50,
        justifyContent: 'center',
        alignItems: 'center', 
       
    },

    buttonText: {
        fontSize: 16,
        fontWeight: 'bold',
        // color: 'black',
    },

    
    localNotification: {
        backgroundColor: 'white',
        borderRadius: 5,
        width: 300,
        height: 50,
        top: -270,
        right: 10,
        justifyContent: 'flex-start',
        alignContent: 'flex-start',
        shadowColor: 'black',
        shadowOpacity: 1,
        elevation: 4,
        shadowRadius: 4,
        shadowOffset: {
            width: 0,
            height: 4
        },
        
    },

    localNotificationText: {
        fontSize: 14,
        fontWeight: 'bold',
        paddingLeft: '5%'
    }
})



export default ExerciseDetailWidget;