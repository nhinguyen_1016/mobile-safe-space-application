import React, { useEffect, useState, useRef } from 'react';
import { View, Text, Image, Platform, StatusBar, SafeAreaView, TouchableOpacity, Pressable, FlatList, Button, TouchableHighlight } from 'react-native';
import ExerciseWidget from '../components/ExerciseWidget';

import { StyleSheet } from 'react-native';
import { LinearGradient } from "expo-linear-gradient";


import images from '../data/images';

import * as Notifications from 'expo-notifications'
import NotificationPopup from 'react-native-push-notification-popup'

import {store} from '../redux/store';
import { useSelector, useDispatch } from 'react-redux';
import { resetDailyMood, resetDailyMoodPoint, resetDailyDoneCustomActivities, setCategoryKey } from '../redux/actions';
import AsyncStorage from '@react-native-async-storage/async-storage';

Notifications.setNotificationHandler({
  handleNotification: async () => ({
    shouldShowAlert: true,
    shouldPlaySound: false,
    shouldSetBadge: false,
  })
})

function Homescreen({ navigation, route }) {

    const notificationData = require('../data/localNotifications.json');
    const [notification, setNotification] = useState(false)
    const notificationListener = useRef()
    const responseListener = useRef()

    //useSelectorHook: wenn eine Action dispatch wird -> vergleicht alten Wert aus Store mit neuem Wert
    //wird in unteren useEffect-Hooks benutzt, um die Komponente bei Änderungen in dem Stores neu zu rendern
    const storeCategories = useSelector(state => state.categoryReducer.categories)
    const storeBadges = useSelector(state => state.badgeReducer.badges)
    const [dateObject, setDateObject] = useState('') 

    //führt Actions zu dem Store durch
    const dispatch = useDispatch()

    const dailyThemeData = require('../data/dailyThemes.json'); 

    //zum Rendern einer FlatList Komponente -> scrollbare UI Komponente
    const [mainContent, setMainContent] = useState([
        { key: '', name: '', id: '', content: '', stackName: ''},
        { key: '1', name: 'Deine heutigen Aktivitäten' },
        { key: '2', name: 'Physisch', stackName: 'physical_screen' },
        { key: '3', name: 'Mental', stackName: 'mental_screen' },
        { key: '4', name: 'Sozial', stackName: 'social_screen' },
               
    ])

    const schedulePushNotification = async (badgeKey) =>  {
    
        let titleData
        let bodyData

        if(badgeKey == '2'){
            titleData = notificationData.MH_3_badge.title
            bodyData = notificationData.MH_3_badge.body

        }

        if(badgeKey == '1'){
            titleData = notificationData.MH_2_badge.title
            bodyData = notificationData.MH_2_badge.body
            
        }
        await Notifications.scheduleNotificationAsync({
          content: {
            title: titleData,
            body: bodyData,
          },
          trigger: {seconds: 3},
        })
    
        popup.show({
          onPress: function() {
            navigation.navigate('badges_screen')
          },
          appTitle: 'Safe Space Anwendung',
          timeText: '',
          title: titleData,
          body: bodyData,
          slideOutTime: 3000,
        })
      }

      const renderCustomPopup = ({ appIconSource, appTitle, timeText, title, body }) => (
        <View style= {styles.localNotification}>
            <Text style={styles.localNotificationText}>
                {title}
            </Text>
            <Text style={[styles.localNotificationText, {fontWeight: 'normal'}]}>
                {body}
            </Text>
        </View>
    );

    //Funktion, um tägliches Thema je nach Datum zu rendern
    const handleDailyRender =  () => {

        const dailyTheme = {
            key: '0', 
            name: '',
            id: '',
            content: '',
            stackName: 'daily_theme_screen'
        }

        const currentDate = new Date().toLocaleDateString()

        switch(JSON.stringify(currentDate)){

            case JSON.stringify('23.8.2023'):
                dailyTheme.name = dailyThemeData.dailyTheme1.title
                dailyTheme.id = dailyThemeData.dailyTheme1.id
                dailyTheme.content = dailyThemeData.dailyTheme1.content
                break;

            case JSON.stringify('24.8.2023'):
                dailyTheme.name = dailyThemeData.dailyTheme1.title
                dailyTheme.id = dailyThemeData.dailyTheme1.id
                dailyTheme.content = dailyThemeData.dailyTheme1.content
                break;

            case JSON.stringify('25.8.2023'):
                dailyTheme.name = dailyThemeData.dailyTheme1.title
                dailyTheme.id = dailyThemeData.dailyTheme1.id
                dailyTheme.content = dailyThemeData.dailyTheme1.content
                break;

            case JSON.stringify('26.8.2023'):
                dailyTheme.name = dailyThemeData.dailyTheme2.title
                dailyTheme.id = dailyThemeData.dailyTheme2.id
                dailyTheme.content = dailyThemeData.dailyTheme2.content
                break;

            case JSON.stringify('27.8.2023'):
                dailyTheme.name = dailyThemeData.dailyTheme3.title
                dailyTheme.id = dailyThemeData.dailyTheme3.id
                dailyTheme.content = dailyThemeData.dailyTheme3.content
                break;

            case JSON.stringify('28.8.2023'):
                dailyTheme.name = dailyThemeData.dailyTheme4.title
                dailyTheme.id = dailyThemeData.dailyTheme4.id
                dailyTheme.content = dailyThemeData.dailyTheme4.content
                break;

            case JSON.stringify('29.8.2023'):
                dailyTheme.name = dailyThemeData.dailyTheme5.title
                dailyTheme.id = dailyThemeData.dailyTheme5.id
                dailyTheme.content = dailyThemeData.dailyTheme5.content
                break;
            
            //default case für Tage außerhalb des Playtestings
            default: 
                dailyTheme.name = dailyThemeData.dailyTheme1.title
                dailyTheme.id = dailyThemeData.dailyTheme1.id
                dailyTheme.content = dailyThemeData.dailyTheme1.content
                break;
         }  
       
        //da der state in React Native niemals direkt mutiert/verändert werden soll:
        //-> Kopieren des Originalstates 
        //->Hinzufügen des heutigen Themas in dem State an obersten Stelle
        //-> Zuweisung des neuen Arrays zu Originalstate
        let newMainContent = [...mainContent]
        let data = {...newMainContent[0]}
        data = dailyTheme
        newMainContent[0] = dailyTheme
        
        setMainContent(newMainContent)

        storeCategories.map((category) => {
            setMainContent(mainContent => [...mainContent, category])

        })

    }

    //verantwortlich für das Erkennen eines neuen Tages
    const handleDayChange = async () => {

        let storedDate = await AsyncStorage.getItem('storedDate')
        if(!storedDate){
            console.log("Homescreen: \n No day currently saved in storage. Set new date.")
            
            try{
                await AsyncStorage.setItem('storedDate', new Date().toLocaleDateString())
                setDateObject(storedDate)
            }catch(e){
                console.log(e)
            }

        }else{
            console.log("Homescreen: \n Date available in storage. Saved date is: " + storedDate)

            let newDate = new Date().toLocaleDateString()
            setDateObject(storedDate)

            //sobald ein neuer Tag beginnt -> Zurücksetzen der Stimmung und gemachten Aktivitäten, um diese wieder auszuführen
            if(storedDate < newDate) {

                dispatch(resetDailyMood())
                dispatch(resetDailyMoodPoint())
                dispatch(resetDailyDoneCustomActivities())
                console.log("New day available! Reset daily data.")

                storedDate = await AsyncStorage.setItem('storedDate', new Date().toLocaleDateString())
                setDateObject(storedDate)
                console.log("New date is: " + storedDate)
            }else{
                console.log("Still the same day.")
                setDateObject(storedDate)

            }
        }
        
    }

    //sobald eine neue Kategorie erstellt wird -> Hinzufügen zu State Array ->  Homescreen neu gerendered
    //Kategorien aus dem Store gilt hier als Dependency für das neue Rendern 
    //-> [storeCategories]
    useEffect(() => {

        //Parameter, die von der Navigation des CreateCategoryScreens kommen
        if(route.params){
            const category = {
                key: route.params.key, name: route.params.name, stackName: 'category_detail_screen'
            }

            console.log(JSON.stringify(category))

            setMainContent(mainContent => [...mainContent, category])

            console.log("Homescreen: Store state(categories): " + JSON.stringify(store.getState().categoryReducer.categories))

        }

    }, [storeCategories])


    //Funktion für Benachrichtungen neuer Erfolge
    //useEffect-Hook wird nur beim ersten Rendern ausgeführt
    useEffect(() => {
        let currentDate = new Date().toLocaleDateString()

        //check to see if welcoming badge already exists in store
        if(store.getState().badgeReducer.badges.length !== 0 ){
            console.log("Homescreen: Welcoming badge already exists!")
        }
        else if(JSON.stringify(currentDate) != JSON.stringify('24.8.2023') && store.getState().badgeReducer.badges.length == 0){ 
            schedulePushNotification(2)
            console.log("Homescreen: Welcoming badge does not exist yet!")

        }

        let exercisePoints = store.getState().exerciseReducer.physicalExercisePoints + 
        store.getState().exerciseReducer.mentalExercisePoints + 
        store.getState().exerciseReducer.socialExercisePoints

        let badge = storeBadges.find((badge) => badge.key == '1')
        if(!badge && exercisePoints >= 8){
            schedulePushNotification(1)
        }else{

        }
    
        handleDayChange()
        handleDailyRender()
     
      notificationListener.current = Notifications.addNotificationReceivedListener(notification => {
            console.log("NOTIFICAITON RECEIVED")
            setNotification(notification)
        })
                
        responseListener.current = Notifications.addNotificationResponseReceivedListener(response => {
            console.log(response)
            navigation.navigate('badges_screen')
        })

        return () => {
            Notifications.removeNotificationSubscription(notificationListener.current)
            Notifications.removeNotificationSubscription(responseListener.current)
        }
    }, [])

    //Rendern der UI Komponenten
    return (
        <SafeAreaView style={styles.screenView}>

            <LinearGradient
                style={styles.gradientBackground}
                locations={[0, 0.99, 0.99]}
                colors={["#e28282", "rgba(243, 162, 57, 0.42)", "rgba(255, 185, 4, 0)"]}
            />

            <View style={styles.mainContentView}>

                <FlatList
                    data={mainContent}
                    extraData={storeCategories}
                    keyExtractor={item => item.key}
                    showsHorizontalScrollIndicator={false}
                    renderItem={({ item, index }) => {
                        //navigiert zu heutigem Thema
                        //Parameter des heutigen Themas wird der Navigation zu DailyThemeScreen übergeben,
                        //um dort gerendered zu werden
                        if (index === 0)
                            return (
                                <Pressable
                                    style={styles.dailyThemeView}
                                    onPress={() => navigation.navigate(item.stackName, {
                                        id: item.id,
                                        title: item.name,
                                        content: item.content
                                    })}
                                >
                                    <Image
                                        style={styles.pictureRecIcon}
                                        contentFit="cover"
                                        source={images.dailyTheme1Rect}
                                    />
                                    <Text style={styles.normalText}>
                                        {item.name}
                                    </Text>
                                </Pressable>
                            )

                        if (index === 1)
                            return <Text style={styles.exerciseText} > {item.name} </Text>

                        //Widgets für physische, mentale und soziale  Aktivitäten
                        if (index > 1 && index < 5)
                            return <ExerciseWidget
                                name={item.name}
                                stackName={item.stackName}
                            />

                        //eigene Kategorien, die ab hier hinzugefügt werden
                        if (index >= 5){
                            return <ExerciseWidget
                                    name={item.name}
                                    stackName={'category_detail_screen'}
                                />
                        }


                    }}
                />

            </View>


            <TouchableHighlight 
                activeOpacity={0.5}
                underlayColor={'black'}
                onPressIn={() => navigation.navigate('create_category_screen')}
            >
                <View style = {styles.addCategoryButton}>
                    <Text style= {styles.buttonText}>
                        +
                    </Text>
                </View>
               
            </TouchableHighlight>

            <NotificationPopup
                ref={ref => popup = ref}
                renderPopupContent={renderCustomPopup}
                shouldChildHandleResponderStart={true}
                shouldChildHandleResponderMove={true}
                isSkipStatusBarPadding={true}
            />

        </SafeAreaView>
    );
}

const styles = StyleSheet.create({

    screenView: {
        flex: 1,
        justifyContent: 'center',
        alignContent: 'center',
        paddingTop: Platform.OS === "android" ? StatusBar.currentHeight : 0

    },

    gradientBackground: {
        top: -12,
        left: -18,
        width: '200%',
        height: '200%',
        backgroundColor: "transparent",
        position: "absolute",
    },

    mainContentView: {
        flex: 1,
        justifyContent: 'flex-start',
        alignItems: 'center',
    },

    dailyThemeView: {
        borderRadius: 10,
        width: '100%',
        height: 120,
        justifyContent: "center",
        alignItems: "center",
    },

    pictureRecIcon: {
        height: 120,
        width: "100%",
        position: 'absolute'
    },

    normalText: {
        fontSize: 18,
        
    },

    exerciseText: {
        fontSize: 20,
        marginTop: 60,
        marginBottom: 20,
        marginLeft: 20

    },

    dailyExercisesView: {
        backgroundColor: 'white',
        borderRadius: 10,
        width: 300,
        height: 100,
        justifyContent: "center",
        alignItems: "center",
        margin: 10
    },

    addCategoryButton: {
        backgroundColor: 'white',
        borderRadius: 50,
        width: 50,
        height: 50,
        justifyContent: 'center',
        alignItems: 'center',
        position: 'absolute',
        bottom: 20,
        right: 20,
        shadowColor: 'black',
        shadowOpacity: 1,
        elevation: 4,
        shadowRadius: 4,
        shadowOffset: {
            width: 0,
            height: 4
        },
        
    },

    buttonText: {
        fontSize: 20,
        fontWeight: 'bold',
    },

    localNotification: {
        backgroundColor: 'white',
        borderRadius: 5,
        width: 300,
        height: 50,
        justifyContent: 'flex-start',
        alignContent: 'flex-start',
        shadowColor: 'black',
        shadowOpacity: 1,
        elevation: 4,
        shadowRadius: 4,
        shadowOffset: {
            width: 0,
            height: 4
        },
        
    },

    localNotificationText: {
        fontSize: 14,
        fontWeight: 'bold',
        paddingLeft: '5%'
    }
})

export default Homescreen;