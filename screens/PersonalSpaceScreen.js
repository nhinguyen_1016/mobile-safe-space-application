import React, { useEffect, useState, useRef } from 'react';
import { View, Text, SafeAreaView, Pressable, StatusBar, Platform } from 'react-native';

import { StyleSheet } from 'react-native';
import { LinearGradient } from "expo-linear-gradient";

import {store} from '../redux/store';
import { useSelector } from 'react-redux';
import AsyncStorage from '@react-native-async-storage/async-storage';


import * as Notifications from 'expo-notifications'
import NotificationPopup from 'react-native-push-notification-popup'

Notifications.setNotificationHandler({
  handleNotification: async () => ({
    shouldShowAlert: true,
    shouldPlaySound: false,
    shouldSetBadge: false,
  })
})


function PersonalSpaceScreen({navigation}) {

    const storeBadges = store.getState().badgeReducer.badges
    const storeMood = useSelector(state => state.moodReducer.mood)
    const [disabled, setDisabled] = useState(false)

    const notificationData = require('../data/localNotifications.json');
    const [notification, setNotification] = useState(false)
    const notificationListener = useRef()
    const responseListener = useRef()


    const schedulePushNotification = async () =>  {
        let notificationTitle
        let notificationBody

        notificationTitle = notificationData.MH_4_badge.title
        notificationBody = notificationData.MH_4_badge.body

        await Notifications.scheduleNotificationAsync({
          content: {
            title: notificationTitle,
            body: notificationBody,
          },
          trigger: {seconds: 1},
        })
    
        popup.show({
          onPress: function() {
            navigation.navigate('badges_screen')
          },
          appTitle: 'Safe Space Anwendung',
          timeText: '',
          title: notificationTitle,
          body: notificationBody,
          slideOutTime: 3000,
        })
      }

      const renderCustomPopup = ({ appIconSource, appTitle, timeText, title, body }) => (
        <View style= {styles.localNotification}>
            <Text style={styles.localNotificationText}>
                {title}
            </Text>
            <Text style={[styles.localNotificationText, {fontWeight: 'normal'}]}>
                {body}
            </Text>
        </View>
    );

    useEffect(() => {

        notificationListener.current = Notifications.addNotificationReceivedListener(notification => {
            console.log("NOTIFICAITON RECEIVED")
            setNotification(notification)
        })
                
        responseListener.current = Notifications.addNotificationResponseReceivedListener(response => {
            console.log(response)
            navigation.navigate('badges_screen')
        })

        return () => {
            Notifications.removeNotificationSubscription(notificationListener.current)
            Notifications.removeNotificationSubscription(responseListener.current)
        }

    }, [])

    
    useEffect(() => {
        console.log("Store state: " + storeMood)

        if(store.getState().moodReducer.mood.length == 0) {
            setDisabled(false)
        }else{
            setDisabled(true)
        }

        let todaysMoodPoints = store.getState().moodReducer.moodPoints
        let badge = storeBadges.find((badge) => badge.key == '3')

        if(todaysMoodPoints){
            console.log("PersonalSpaceScreen: Todays mood registered.")
            if(!badge && todaysMoodPoints <= -1 && new Date().toLocaleDateString('en-US') >= '8/29/2023' ){
                console.log("PersonalSpaceScreen: Mental badge doesn't exist yet! Send notification soon .")
                schedulePushNotification()

            }else{
                console.log("PersonalSpaceScreen: Mental badge  already exists!")
            }

        }else{
            console.log("PersonalSpaceScreen: Todays mood not registered yet.")
        }

    }, [storeMood])

    return (
        <SafeAreaView style={styles.screenView}>

            <LinearGradient
                style={styles.gradientBackground}
                locations={[0, 0.99, 0.99]}
                colors={["#e28282", "rgba(243, 162, 57, 0.42)", "rgba(255, 185, 4, 0)"]}
            />

            <View style={styles.mainContentView}>
                <Pressable
                    style={[styles.feelingsView, disabled ? {backgroundColor: '#d3d3d3'} : {backgroundColor: 'white'}]}
                    onPress={disabled ? null : () => navigation.navigate("daily_feelings_screen")}
                >
                    <Text style={styles.normalText}>
                        Es ist wichtig, dir regulär Zeit für deine Gefühle zu nehmen.
                        Wie fühlst du dich heute am Ende des Tages? 
                    </Text>
                </Pressable>

                {store.getState().moodReducer.mood !== 0 ? 
                    <View style={styles.moodView}>

                        <Text style={styles.moodText} 
                            // onPress={handleStorageCheck}
                        >
                            Deine heutigen Gefühle:
                        </Text>

                       {storeMood.map(currentMood => {
                            return(
                                <Text style={styles.moodText} key={currentMood}>
                                    {currentMood},
                                </Text>
                        )
                       })}
                    </View> 

                    : console.log("No mood currently saved.")
                }

            </View>

            <NotificationPopup
                ref={ref => popup = ref}
                renderPopupContent={renderCustomPopup}
                shouldChildHandleResponderStart={true}
                shouldChildHandleResponderMove={true}
                isSkipStatusBarPadding={true}
            />

        </SafeAreaView>

    );
}

const styles = StyleSheet.create({

    screenView: {
        flex: 1,
        justifyContent: 'center',
        alignContent: 'center',
        paddingTop: Platform.OS === "android" ? StatusBar.currentHeight : 0

    },

    gradientBackground: {
        top: -12,
        left: -18,
        // width: 388,
        // height: 664,
        width: '200%',
        height: '200%',
        backgroundColor: "transparent",
        position: "absolute",
    },

    mainContentView: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
        gap: 20
    },

    feelingsView: {
        borderRadius: 10,
        width: 300,
        height: 200,
        justifyContent: "center",
        alignItems: "center",
        padding: 30,
        shadowColor: 'black',
        shadowOpacity: 1,
        elevation: 4,
        shadowRadius: 4,
        shadowOffset: {
            width: 0,
            height: 4
        }   
    },

    moodView: {
        backgroundColor: 'white',
        borderRadius: 10,
        width: 300,
        height: 100,
        flexDirection: 'row',
        flexWrap: 'wrap',
        justifyContent: "center",
        alignItems: "center",
        alignContent: "center",
        paddingLeft: 2,
        paddingRight: 2,
        gap: 5,
        shadowColor: 'black',
        shadowOpacity: 1,
        elevation: 4,
        shadowRadius: 4,
        shadowOffset: {
            width: 0,
            height: 4
        }   
    },

    normalText: {
        fontSize: 20
    },

    moodText: {
        fontSize: 16
    },

    localNotification: {
        backgroundColor: 'white',
        borderRadius: 5,
        width: 300,
        height: 50,
        justifyContent: 'flex-start',
        alignContent: 'flex-start',
        shadowColor: 'black',
        shadowOpacity: 1,
        elevation: 4,
        shadowRadius: 4,
        shadowOffset: {
            width: 0,
            height: 4
        },
        
    },

    localNotificationText: {
        fontSize: 14,
        fontWeight: 'bold',
        paddingLeft: '5%'
    }


})

export default PersonalSpaceScreen;