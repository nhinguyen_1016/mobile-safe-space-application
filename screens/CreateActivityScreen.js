import React, {useEffect, useState} from 'react';
import { View, Text, SafeAreaView, FlatList } from 'react-native';
import { useNavigation } from '@react-navigation/native';

import { StyleSheet } from 'react-native';
import { LinearGradient } from "expo-linear-gradient";

import DetailImage from '../components/DetailImage';
import DetailDescription from '../components/DetailDescription';

function CreateActivityScreen({route}) {

    const [mainContent, setMainContent] = useState([
        { key: '0', name: 'ExercisePic', type: 'customActivity'},
        { key: '1', name: 'Main Content', type: 'customActivity' },
    ])

    return (
        
        <SafeAreaView style={styles.screenView}>
            <View style={styles.mainContentView}>

                <FlatList
                    data={mainContent}
                    keyExtractor={item => item.key}
                    showsHorizontalScrollIndicator={false}
                    renderItem={({ item, index }) => {
                        if (index === 0)
                            return <DetailImage 
                                type={!route.params ? item.type : route.params.type}
                            />
                      
                        if (index === 1)
                            return (
                        // von dem CustomCategoryScreen aus wird der titel der Kategorie als Parameter mit übergeben
                                <DetailDescription
                                    type={!route.params ? item.type : route.params.type}
                                    category={route.params ? route.params.category : ''}
                                />
                            )   
                    }
                }
                />

            </View>
        </SafeAreaView>
    );
}


const styles = StyleSheet.create({

    screenView: {
        flex: 1,
        justifyContent: 'center',
        alignContent: 'center',
    },

    gradientBackground: {
        top: -12,
        left: -18,
        // width: 388,
        // height: 664,
        width: '200%',
        height: '200%',
        backgroundColor: "transparent",
        position: "absolute",
    },

    mainContentView: {
        flex: 1,
        justifyContent: 'flex-start',
        alignItems: 'center',
    },

    dailyThemePic: {
        width: 360,
        // width: '200%',
        height: 250,
            
    },
   
})

export default CreateActivityScreen;