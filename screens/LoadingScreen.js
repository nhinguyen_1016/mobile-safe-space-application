import React, { useEffect } from 'react';
import { StyleSheet, View, ImageBackground, Text } from 'react-native';

function LoadingScreen({ navigation }) {

  useEffect(() => {
    setTimeout(() => {
      navigation.replace('home_screen')
    }, 3000)
  }, [])

  return (
    <View style={styles.loadingscreen}>
      <ImageBackground
        style={styles.appLogoIcon}
        resizeMode="cover"
        source={require("../assets/applogo.png")}
      />
      <Text style={styles.safeSpace}>Safe Space</Text>
    </View>
  );
}

const styles = StyleSheet.create({
  loadingscreen: {
    backgroundColor: 'white',
    flex: 1,
    justifyContent: 'center',
    alignItems: "center",
  },
  appLogoIcon: {
    width: 169,
    height: 169,
  },
  safeSpace: {
    fontSize: 24,
    fontWeight: "700",
    fontFamily: 'Montserrat',
    color: '#E28282',
  },

});

export default LoadingScreen;