import React, { useEffect, useState } from 'react';
import { View, Text, SafeAreaView, Platform, StatusBar, Image, Dimensions, FlatList} from 'react-native';

import {store} from '../redux/store';
import { useSelector, useDispatch } from 'react-redux';
import { setBadge } from '../redux/actions';

import { StyleSheet } from 'react-native';
import { LinearGradient } from "expo-linear-gradient";
import images from '../data/images';

function BadgesScreen() {

    const dispatch = useDispatch()

    const storePhysicalActivities = useSelector(state => state.exerciseReducer.physicalExercises)
    const storeMentalActivities = useSelector(state => state.exerciseReducer.mentalExercises)
    const storeSocialActivities = useSelector(state => state.exerciseReducer.socialExercises)
    const storeMoodPoints = useSelector(state => state.moodReducer.moodPoints)
    const storeBadges = store.getState().badgeReducer.badges



    //das Laden aus der badges.json Datei wird hier nicht gebraucht
    const [badges, setBadges] =  useState([
        {
            key: '0', name: 'Be kind to yourself', source: images.unlocked_badge_MH_1, unlocked: false 
        },
        {
            key: '1', name: 'It is okay to recharge', source: images.unlocked_badge_MH_2, unlocked: false 
        },
        {
            key: '2', name: 'Take care of yourself', source: images.unlocked_badge_MH_3, unlocked: false
        },
        {
            key: '3', name: 'Healing takes time', source: images.unlocked_badge_MH_4,unlocked: false
        },
        {
            key: '4', name: 'Work hard', source: images.unlocked_badge_PH_1, unlocked: false 
        },
        {
            key: '5', name: 'Beast mode', source: images.unlocked_badge_PH_2, unlocked: false
        },
       
        
    ])


    const handlePhysicalBadges = () => {
        //physical badge Lv.1

        //sobald bestimmte Punkte erreicht sind -> unlocked-property des Erfolges wird im State auf true gesetzt 
        //Rendern des Erfolgs 
        //dafür wird Badge erstmal in dem Array gesucht und verändert
        if(store.getState().exerciseReducer.physicalExercisePoints >= 2){
            let newArray = [...badges]
            let currentData = {...newArray[4]}
            currentData.unlocked = true
            newArray[4] = currentData

            setBadges(newArray)

            //neue Erfolge werden global in dem Store gespeichert
            //(durch Redux-Persist in reducer.js erfolgt die persisten Speicherung des Erfolges auf dem Gerät)
            let badge = storeBadges.find((badge) => badge.key == '4')
            if(store.getState().exerciseReducer.physicalExercisePoints == 2 && !badge){
                console.log("Badgesscreen: Physical badge LV.1 doesn't exist yet. Add to store!")
                
                newArray = newArray.filter(badge => badge.unlocked == true)
                    .map(badge => 
                        dispatch(setBadge(badge)))
            
            //falls Badge schon in dem Store ist -> kein neues Freischalten
            }else{
                    console.log("Badgesscreen: Physical badge LV.1 already exists!")            
            }

            console.log("BadgesScreen: New badges in store: " + JSON.stringify(store.getState().badgeReducer.badges))
        }
        
        //physical badge Lv.2
        if(store.getState().exerciseReducer.physicalExercisePoints >= 4){
            let newArray = [...badges]
            let currentData = {...newArray[5]}
            currentData.unlocked = true
            newArray[5] = currentData

            setBadges(newArray)


            let badge = storeBadges.find((badge) => badge.key == '5')
            if(store.getState().exerciseReducer.physicalExercisePoints == 4 && !badge){
                console.log("Badgesscreen: Physical badge LV.2 doesn't exist yet. Add to store!")

                newArray = newArray.filter(badge => badge.unlocked == true)
                .map(badge => 
                    dispatch(setBadge(badge)))

            }else{
                console.log("Badgesscreen: Physical badge LV.2 already exists!")
            }
            
            console.log("BadgesScreen: New badges in store: " + JSON.stringify(store.getState().badgeReducer.badges))
        
        }
    }

    const handleMentalBadges = () => {

       //mental badge Lv.0 (Take care of yourself)
        let badge = storeBadges.find((badge) => badge.key == '2')

        let newArray = [...badges]
        let currentData = {...newArray[2]}
        currentData.unlocked = true
        newArray[2] = currentData

        setBadges(newArray)

        let currentDate = new Date().toLocaleDateString()
        //für Testzwecke wird das Datum ungleich auf den 25.8.2023 gesetzt, um Erfolg außerhalb des Testens trotzdem freizuschalten
        if(JSON.stringify(currentDate) != JSON.stringify('25.8.2023') && !badge){ 
            console.log("BadgesScreen: Mental badge Lv.0 doesn't exist yet. Add to store!")

            newArray = newArray.filter(badge => badge.unlocked == true)
                    .map(badge => 
                        dispatch(setBadge(badge)))

        }else{
            console.log("BadgesScreen: Mental badge Lv.0 already exists!")
        }

        console.log("BadgesScreen: Badges in store: " + JSON.stringify(store.getState().badgeReducer.badges))



        // //mental badge Lv.1 (It is okay to recharge)

        let exercisePoints = store.getState().exerciseReducer.physicalExercisePoints + 
        store.getState().exerciseReducer.mentalExercisePoints + 
        store.getState().exerciseReducer.socialExercisePoints

        if(exercisePoints >= 8){

            let newArray2 = [...badges]
            let currentData2 = {...newArray[1]}
            currentData2.unlocked = true
            newArray2[1] = currentData2
    
            setBadges(newArray2)
    
            let badge2 = storeBadges.find((badge) => badge.key == '1')
            if(!badge2 && exercisePoints >= 8 ){
                console.log("BadgesScreen: Mental badge doesn't exist yet! Add to store .")

                newArray2 = newArray2.filter(badge => badge.unlocked == true)
                .map(badge => 
                    dispatch(setBadge(badge)))
            }else{

            }
        }

        
    }

    const handleMentalBadges2 = () => {
         // mental badge Lv.2 (Healing takes time) ->
                 //für Testzwecke wird das Datum mindestens auf den 29.8.2023 gesetzt, um Erfolg außerhalb des Testens trotzdem freizuschalten
 
         if(store.getState().moodReducer.moodPoints <= -1 && new Date().toLocaleDateString() >= '29.8.2023' ){
            let newArray = [...badges]
            let currentData = {...newArray[3]}
            currentData.unlocked = true
            newArray[3] = currentData
    
            setBadges(newArray)
    
            let badge = storeBadges.find((badge) => badge.key == '3')
                    //für Testzwecke wird das Datum mindestens auf den 29.8.2023 gesetzt, um Erfolg außerhalb des Testens trotzdem freizuschalten

            if(!badge && store.getState().moodReducer.moodPoints <= -1 && new Date().toLocaleDateString() >= '29.8.2023' ){
                console.log("BadgesScreen: Mental badge doesn't exist yet! Add to store .")

                newArray = newArray.filter(badge => badge.unlocked == true)
                .map(badge => 
                    dispatch(setBadge(badge)))
            }else{

            }
        }
    }

    const handleMentalBadges3 = () => {
          //mental badge Lv.3 Be kind to yourself
          if(store.getState().exerciseReducer.mentalExercisePoints >= 3 ){
            let newArray = [...badges]
            let currentData = {...newArray[0]}
            currentData.unlocked = true
            newArray[0] = currentData
    
            setBadges(newArray)
    
            let badge = storeBadges.find((badge) => badge.key == '0')
            if(!badge && store.getState().exerciseReducer.mentalExercisePoints == 3){
                console.log("BadgesScreen: Mental badge doesn't exist yet! Add to store .")

                newArray = newArray.filter(badge => badge.unlocked == true)
                .map(badge => 
                    dispatch(setBadge(badge)))
            }else{

            }
        }

    }

    useEffect(() => {
        handleMentalBadges()

    }, [])

    //wird ausgeführt, falls sich die Stimmungspunkte in dem Store verändert
    useEffect(() => {
        handleMentalBadges2()

    }, [storeMoodPoints])

    //wird ausgeführt, falls neue mentale Aktivitäten abgeschlossen und dem Store hinzugefügt wurden

    useEffect(() => {
        handleMentalBadges3()

    }, [storeMentalActivities])


    //wird ausgeführt, falls neue physische Aktivitäten abgeschlossen und dem Store hinzugefügt wurden

    useEffect(() => {
        handlePhysicalBadges()
    }, [storePhysicalActivities])


    return (
        <SafeAreaView style={styles.screenView} >

            <LinearGradient
                style={styles.gradientBackground}
                locations={[0, 0.99, 0.99]}
                colors={["#e28282", "rgba(243, 162, 57, 0.42)", "rgba(255, 185, 4, 0)"]}
            />
            <View style={styles.mainContentView} >

            <FlatList
                columnWrapperStyle={styles.flatListView}
                numColumns={5}
                data={badges}
                keyExtractor={item => item.key}
                showsHorizontalScrollIndicator={false}
                renderItem={({ item, index }) => {                
                        // let image = handleImage()

                        //Vergleich zwischen Erfolgen in State und Store
                        //identischer Erfolg -> Badge wurde schon freigeschaltet, kein neues Freischalten oder Rendern
                        let currentData;
                        if(storeBadges.length !== 0 ){
                            // console.log("BadgesScreen: \n store badges array is not empty. ")
                            currentData = storeBadges.find(badge => badge.name === item.name)
                                if(currentData) {
                                    // console.log("Match found! " + JSON.stringify(currentData))
                                }else{
                                    // console.log("No match found!")
                                }
                        }else{
                            // console.log("BadgesScreen: \n store badges array still empty so far.")
                        } 

                        return( 
                            currentData ? 
                                <Image
                                    style={styles.image}
                                    resizeMode="cover"
                                    source={item.source}
                                    key={index}
                                /> 

                            : <View style= {styles.lockedImage}>
                                 <Text style={styles.lockedImageText}>
                                    ?
                                 </Text>
                            </View>
                        )
                }}
            />
               
            </View>
        </SafeAreaView>   
    );
}

const styles = StyleSheet.create({ 
    screenView: {
        flex: 1,
        justifyContent: 'center',
        alignContent: 'center',
        paddingTop: Platform.OS === "android" ? StatusBar.currentHeight : 0

    },

    gradientBackground: {
        top: -12,
        left: -18,
        // width: 388,
        // height: 664,
        width: '200%',
        height: '200%',
        backgroundColor: "transparent",
        position: "absolute",
    },

    mainContentView: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
    },

    flatListView: {
        paddingLeft: '5%',
        paddingRight: '5%',
        justifyContent: 'center',
        alignItems: 'flex-start',
        flexDirection: 'row',
        alignContent: 'flex-start',
        flexWrap: 'wrap',
        gap: 20,
            
    },

    image: {
        height: 140,
        width: 130,
        margin: 10,
    },

    lockedImage: {
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: 'white',
        height: 140,
        width: 140,
        marginBottom: 10,
        borderRadius: 100,
        shadowColor: 'black',
        shadowOpacity: 1,
        elevation: 4,
        shadowRadius: 4,
        shadowOffset: {
            width: 0,
            height: 4
        },
        
    },

    lockedImageText: {
        fontSize: 50,

    }

})

export default BadgesScreen;