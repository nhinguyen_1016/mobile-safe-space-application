import React, { useState } from 'react';
import { View, Text, Image, Pressable, SafeAreaView, StatusBar, Platform, FlatList, ImageBackground, Dimensions } from 'react-native';
import { StyleSheet } from 'react-native';

import DetailImage from '../components/DetailImage';
import DetailDescription from '../components/DetailDescription';

const dailyThemeData = require('../data/dailyThemes.json'); 

function DailyThemeScreen({route, navigation}) {

    const [mainContent, setMainContent] = useState([
        { key: '0', name: 'DailyThemePic'},
        { key: '1', name: 'Main Content' },
       
    ])

    const {id, title, content} = route.params

    //Type benötigt, um in DetailDescription.js Rendern je nach Thema oder Aktivität zu unterscheiden
    var type = 'dailyTheme';

    
    return (
        <SafeAreaView style={styles.screenView}>
            <View style={styles.mainContentView}>

                <FlatList
                    data={mainContent}
                    keyExtractor={item => item.key}
                    showsHorizontalScrollIndicator={false}
                    renderItem={({ item, index }) => {

                        if (index === 0)
                            return <DetailImage type={type}/>
                            
                        if (index === 1)
                            return (
                              <DetailDescription
                                title={title}
                                description={content}
                              />
                            )
                    }}
                />

            </View>
        </SafeAreaView>
    );
}

const styles = StyleSheet.create({

    screenView: {
        flex: 1,
        justifyContent: 'center',
        alignContent: 'center',
    },

    mainContentView: {
        flex: 1,
        justifyContent: 'flex-start',
        alignItems: 'center',
    },

    dailyThemePic: {
        width: 360,
        // width: '200%',
        height: 250,
            
    },
   
})

export default DailyThemeScreen;