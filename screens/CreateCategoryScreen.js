import React, { useEffect, useState} from 'react';

import { View, Text, SafeAreaView, Platform, StatusBar, TextInput, TouchableOpacity, FlatList, Pressable} from 'react-native';
import { StyleSheet } from 'react-native';

import {useSelector, useDispatch} from 'react-redux'
import { setActivity, setCategory, setActivityKey, setCategoryKey } from '../redux/actions';
import {store} from '../redux/store';

import { useNavigation } from '@react-navigation/native';

function CreateCategoryScreen({route}) {

    const [categoryTitle, setCategoryTitle] = useState("")
    const [category, setCategories] = useState({})
    const [currentActivities, setCurrentActivities] = useState([])
    const [counter, setCounter] = useState(3)
  

    const [mainContent, setMainContent] =  useState([
        {key: '0', name: 'Title Input'},
        {key: '1', name: 'Creating activity'},
        {key: '2', name: 'Save category'}
    ])

    const dispatch = useDispatch()
    const navigation = useNavigation()

    useEffect(() => {

        if(!route.params){

        }else{
            if(!route.params.editable){
                console.log("CreateCategoryScreen: Route params(creating): " + JSON.stringify(route.params))

                //wird hier von dem CreateActivityScreen  bzw. DetailDescription.js aus navigiert, nachdem eine neue Aktivität erstellt wurde
                //Hinzufügen zu State und Rendern der Aktivität in dieser Komponente
                const {title, description} = route.params
                const activityData = {
                    key: store.getState().activityReducer.activityKey, index: store.getState().activityReducer.activityKey, category: categoryTitle, title: title, id: "CustomActivity_" + title , description: description, done: false

                }
                console.log("CreateCategoryScreen: New activity: " + JSON.stringify(activityData))

                setCurrentActivities(currentActivities => [...currentActivities, activityData])
                setMainContent(mainContent => [...mainContent, activityData])

                dispatch(setActivityKey())

                //Editierfunktion wurde nicht umgesetzt
            // }else{
            //     console.log("CreateCategoryScreen: Route params(editing): " + JSON.stringify(route.params))
            //     const {title, description, index} = route.params

            //     let newData = [...mainContent]
            //     let currentActivity = {...newData[index]}
            //     currentActivity.name = title
            //     currentActivity.content = description

            //     newData[index] = currentActivity

            //     setMainContent(newData)


            //     let newData2 = [...currentActivities]
            //     let currentActivity2 = {...newData2[index]}
            //     currentActivity2.name = title
            //     currentActivity2.content = description

            //     newData2[index] = currentActivity2

            //     setCurrentActivities(newData2)
            //     console.log("CreateCategoryScreen: New edited activities" +JSON.stringify(currentActivities))

            }
        }
    }, [route.params])


    const handleOnCreate = () => {
        navigation.navigate('create_activity_screen')
    }

    //Beim Speichern der Kategorie wird diese mitsamt der Aktivitäten in den Store mit dispatch gespeichert
    const handleOnSave = () => {

        const newArray = [...currentActivities]
        newArray.map(activity => {
            dispatch(setActivity(activity))
        })

        console.log("CreateCategory: handleOnSave() dispatch every activity: " + JSON.stringify(newArray))

        const category = {
            key: store.getState().categoryReducer.categoryKey, name: categoryTitle
        }

        dispatch(setCategory(category, newArray))

        console.log("CategoryScreen: handleOnSave() Store state category: " + JSON.stringify(store.getState().categoryReducer.categories))
        console.log("CategoryScreen: handleOnSave() Store state currentActivities: " + JSON.stringify(store.getState().activityReducer.activities))


        //es wird anschließend zum Homescreen navigiert, um die neue Kategorie dort zu rendern
        //als Paramater an den Homescreen wird der Schlüssel und Name der Kategorie mitgegeben
        navigation.navigate('home_screen', {
            key: store.getState().categoryReducer.categoryKey,  
            name: categoryTitle
        })

        dispatch(setCategoryKey())
        console.log("CreateCategoryScreen: Pushed category key")

    }

    const renderItem = ({item, index}) => {

        if (index == 0)
            return <TextInput
                    key={item.key}
                    style={styles.nameContent}
                    placeholder='Name'
                    maxLength={40}
                    onChangeText={(value) => {setCategoryTitle(value)}}
                />
  
        if (index == 1)
            return <TouchableOpacity 
                        key={item.key}
                        style={[styles.addButton,  categoryTitle == 0 ? {backgroundColor: '#d3d3d3'} : {backgroundColor: 'white'}]} 
                        disabled={categoryTitle == 0 ? true : false} 
                        onPress={handleOnCreate}
                    >
                        <Text style={[styles.buttonText, categoryTitle == 0 ? {color: 'white'} : {color: 'black'}]} >
                            Aktivität hinzufügen
                        </Text>   
                    </TouchableOpacity>
            
        if (index == 2)
            return <TouchableOpacity 
                        key={item.key}
                        style={[styles.saveButton, categoryTitle == 0 ? {backgroundColor: '#d3d3d3'} : {backgroundColor: '#e28282'} ]} 
                        disabled={categoryTitle == 0 ? true : false} 
                        onPress={handleOnSave}
                    >
                        <Text style={[styles.buttonText, {color: 'white', fontWeight: 'bold'}]} >
                            Speichern
                        </Text>   
                </TouchableOpacity>

        if (index > 2)
            return <Pressable 
                        key={item.index}
                        style={styles.activityView} 
                    >
                        <Text style={{fontSize: 16, color: 'black'}}>
                            {item.title}
                        </Text>

                    </Pressable>

    }

    return (
        <SafeAreaView style={styles.screenView}>
            <View style={styles.mainContentView}>

                {/* scrollbare Liste */}
                <FlatList
                    contentContainerStyle={styles.flatListView}
                    data={mainContent}
                    keyExtractor={item => item.key}
                    showsHorizontalScrollIndicator={false}
                    renderItem={renderItem}
                />

            </View>
        </SafeAreaView>
    );
}

const styles = StyleSheet.create({ 

    screenView: {
        flex: 1,
        justifyContent: 'center',
        alignContent: 'center',
        paddingTop: Platform.OS === "android" ? StatusBar.currentHeight : 0,
        backgroundColor: 'white'

    },

    mainContentView: {
        flex: 1,
        justifyContent: 'flex-start',
        alignItems: 'center',
    },

    flatListView: {
        justifyContent: 'flex-start',
        alignItems: 'center',
        marginTop: 10,
        paddingBottom: 40,
        gap: 30
    },

    nameContent: {
        paddingLeft: 20,
        justifyContent: 'center',
        alignItems: 'center',
        width: 300,
        height: 40,
        borderRadius: 5,
        shadowColor: 'black',
        shadowOpacity: 1,
        elevation: 4,
        shadowRadius: 4,
        shadowOffset: {
            width: 0,
            height: 4
        },
        fontSize: 16
    },

    addButton: {
        borderRadius: 5,
        backgroundColor: 'white',
        width: 200,
        height: 50,
        justifyContent: 'center',
        alignItems: 'center', 
        shadowColor: 'black',
        shadowOpacity: 1,
        elevation: 4,
        shadowRadius: 4,
        shadowOffset: {
            width: 0,
            height: 4
        },  
    },

    saveButton: {
        borderRadius: 5,
        width: 250,
        height: 50,
        justifyContent: 'center',
        alignItems: 'center',  
        marginBottom: 20      
    },

    buttonText: {
        fontSize: 16,
        fontWeight: 'bold',
        color: 'black',
    },

    activityView: {
        backgroundColor: 'white',
        borderRadius: 10,
        width: 250,
        height: 70,
        justifyContent: "center",
        alignItems: "center",
        shadowColor: 'black',
        shadowOpacity: 1,
        elevation: 4,
        shadowRadius: 4,
        shadowOffset: {
            width: 0,
            height: 4
        },
    },
})

export default CreateCategoryScreen;