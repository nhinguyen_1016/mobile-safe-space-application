import React, { useEffect, useState } from 'react';
import { View, Text, SafeAreaView, StatusBar, Platform, TouchableOpacity } from 'react-native';
import FeelingsButton from '../components/FeelingsButton';

import { StyleSheet } from 'react-native';
import { LinearGradient } from "expo-linear-gradient";

import {useSelector, useDispatch} from 'react-redux'
import {store} from '../redux/store';

import {setMoodPoint, setMood } from '../redux/actions';

import AsyncStorage from '@react-native-async-storage/async-storage';
import asyncStorageKeys from '../data/asyncStorageKeys';



function DailyFeelingsScreen({navigation}) {

    const feelings = require('../data/dailyMood.json');

    const [currentMoodPoints, setCurrentMoodPoints] = useState(0);
    const [currentMood, setCurrentMood] = useState([]);
    const [count, setCount]  = useState(0)
    const [disabled, setDisabled] = useState(false)
    // const storeMood = useSelector(state => state.moodReducer.mood)

    const dispatch = useDispatch()

    //Laden jeder Emotion aus JSON-Datei
    const positiveFeelings = [
        {
            key: '0',
            id: feelings.positive_emotions.positiveEmotion1.id,
            emotion: feelings.positive_emotions.positiveEmotion1.emotion,
            points: feelings.positive_emotions.positiveEmotion1.points,
        },
        {
            key: '1',
            id: feelings.positive_emotions.positiveEmotion2.id,
            emotion: feelings.positive_emotions.positiveEmotion2.emotion,
            points: feelings.positive_emotions.positiveEmotion2.points,
        },
        {
            key: '2',
            id: feelings.positive_emotions.positiveEmotion3.id,
            emotion: feelings.positive_emotions.positiveEmotion3.emotion,
            points: feelings.positive_emotions.positiveEmotion3.points,
        },
        {
            key: '3',
            id: feelings.positive_emotions.positiveEmotion4.id,
            emotion: feelings.positive_emotions.positiveEmotion4.emotion,
            points: feelings.positive_emotions.positiveEmotion4.points,
          
        },
        {
            key: '4',
            id: feelings.positive_emotions.positiveEmotion5.id,
            emotion: feelings.positive_emotions.positiveEmotion5.emotion,
            points: feelings.positive_emotions.positiveEmotion5.points,
         
        },

    ]

    const moderateFeelings =[
        {
            key: '0',
            id: feelings.moderate_emotions.moderateEmotion1.id,
            emotion: feelings.moderate_emotions.moderateEmotion1.emotion,
            points: feelings.moderate_emotions.moderateEmotion1.points,

        },
        {
            key: '1',
            id: feelings.moderate_emotions.moderateEmotion2.id,
            emotion: feelings.moderate_emotions.moderateEmotion2.emotion,
            points: feelings.moderate_emotions.moderateEmotion2.points,
            
        },
        {
            key: '2',
            id: feelings.moderate_emotions.moderateEmotion3.id,
            emotion: feelings.moderate_emotions.moderateEmotion3.emotion,
            points: feelings.moderate_emotions.moderateEmotion3.points,
                       
        },
        {
            key: '3',
            id: feelings.moderate_emotions.moderateEmotion4.id,
            emotion: feelings.moderate_emotions.moderateEmotion4.emotion,
            points: feelings.moderate_emotions.moderateEmotion4.points,
           
                    },
        {
            key: '4',
            id: feelings.moderate_emotions.moderateEmotion5.id,
            emotion: feelings.moderate_emotions.moderateEmotion5.emotion,
            points: feelings.moderate_emotions.moderateEmotion5.points,
          
        },

    ]

    const negativeFeelings = [
        {
            key: '0',
            id: feelings.negative_emotions.negativeEmotion1.id,
            emotion: feelings.negative_emotions.negativeEmotion1.emotion,
            points: feelings.negative_emotions.negativeEmotion1.points
        },
        {
            key: '1',
            id: feelings.negative_emotions.negativeEmotion2.id,
            emotion: feelings.negative_emotions.negativeEmotion2.emotion,
            points: feelings.negative_emotions.negativeEmotion2.points
        },
        {
            key: '2',
            id: feelings.negative_emotions.negativeEmotion3.id,
            emotion: feelings.negative_emotions.negativeEmotion3.emotion,
            points: feelings.negative_emotions.negativeEmotion3.points
        },
        {
            key: '3',
            id: feelings.negative_emotions.negativeEmotion4.id,
            emotion: feelings.negative_emotions.negativeEmotion4.emotion,
            points: feelings.negative_emotions.negativeEmotion4.points
        },
        {
            key: '4',
            id: feelings.negative_emotions.negativeEmotion5.id,
            emotion: feelings.negative_emotions.negativeEmotion5.emotion,
            points: feelings.negative_emotions.negativeEmotion5.points
        },

    ]

    //wenn noch keine Emotion in dem Store ausgewählt wurde -> Speichern Button der Stimmung deaktiviert
    useEffect(() => {
        console.log("Current mood points: " + currentMoodPoints)
        console.log("Current mood: " + currentMood.toString())

        if(store.getState().moodReducer.mood.length == 0 && count !== 0){
            console.log("Today's feelings not  registered yet! Enable save button.")

            setDisabled(false)
        }else{
            console.log("Today's feelings already registered! Disable save button.")
            setDisabled(true)
        }

    }, [currentMood])

    //beim Auswählen einer Emotion -> Addieren des Punktes zur Punktestimmung (wichtig für Erfolge)
    const handleMood = (points, emotion, isPressed) => {

        //Auswählen einer Emotion
        if(!isPressed){
            setCount(count + 1)
            setCurrentMoodPoints(currentMoodPoints + points)

            //Hinzufügen einer neuen Emotion am Ende des Arrays
            setCurrentMood(currentMood => [...currentMood, emotion ])
        
            //Löschen einer Emotion -> Emotion wird gesucht und aus dem Array an der Stelle wieder entfernt
        }else{
            const newMood = [...currentMood]
            const index = newMood.indexOf(emotion)
            if(index > -1){
                newMood.splice(index, 1)
                setCurrentMood(newMood)
            }

            setCount(count -1)
            setCurrentMoodPoints(currentMoodPoints - points)
        }
    }

    const handleOnSave = () => {

        dispatch(setMood(currentMood))
        dispatch(setMoodPoint(currentMoodPoints))

        storeData()

        console.log("Mood point in store is: " + store.getState().moodReducer.moodPoints)
        console.log("Mood in store is: " + store.getState().moodReducer.mood)
        navigation.goBack("personal_space_screen", {
            
        })
    }

    const storeData = async () => {

        let moodDayStorage
        let moodPointsStorage
        const currentDate = new Date().toLocaleDateString()

        // switch(JSON.stringify(currentDate)){

        //für Testzwecke außerhalb des Playtestings:
            switch(JSON.stringify('25.8.2023')){

            case JSON.stringify('24.8.2023'):
                moodPointsStorage = asyncStorageKeys.moodPoints_day1_storage
                moodDayStorage = asyncStorageKeys.mood_day1_storage
                break

            case JSON.stringify('25.8.2023'):
                moodPointsStorage = asyncStorageKeys.moodPoints_day1_storage
                moodDayStorage = asyncStorageKeys.mood_day1_storage

                break

            case JSON.stringify('26.8.2023'):
                moodPointsStorage = asyncStorageKeys.moodPoints_day2_storage
                moodDayStorage = asyncStorageKeys.mood_day2_storage

                break

            case JSON.stringify('27.8.2023'):
                moodPointsStorage = asyncStorageKeys.moodPoints_day3_storage
                moodDayStorage = asyncStorageKeys.mood_day3_storage

                break

            case JSON.stringify('28.8.2023'):
                moodPointsStorage = asyncStorageKeys.moodPoints_day4_storage
                moodDayStorage = asyncStorageKeys.mood_day4_storage
                break

            case JSON.stringify('29.8.2023'):
                moodPointsStorage = asyncStorageKeys.moodPoints_day5_storage
                moodDayStorage = asyncStorageKeys.mood_day5_storage

            default:
                moodPointsStorage = asyncStorageKeys.moodPoints_day1_storage
                moodDayStorage = asyncStorageKeys.mood_day1_storage
                break
        }


        //Speichern der Stimmung in den lokalen Speicher -> wichtig für das Rendern in der Timeline
        const dailyMood = {
            "day": new Date().toLocaleDateString(),
            "mood": currentMood
        }

        const dailyMoodPoints = {
            "day": new Date().toLocaleDateString(),
            "moodPoints": currentMoodPoints
        }

        try{
            const firstPair = [moodDayStorage, JSON.stringify(dailyMood)]
            const secondPair = [moodPointsStorage, JSON.stringify(dailyMoodPoints)]
    
            await AsyncStorage.multiSet([firstPair, secondPair])
        }catch(e){
            console.log(e)
        }
        console.log("DailyFeelingsScreen: Successfully saved mood and moodPoints to AsyncStorage!")
    
    }

    return (
       <SafeAreaView style={styles.screenView} >
            <LinearGradient
                style={styles.gradientBackground}
                locations={[0, 0.99, 0.99]}
                colors={["#e28282", "rgba(243, 162, 57, 0.42)", "rgba(255, 185, 4, 0)"]}
            />

            <View style={styles.mainContentView} >

                {/* für jede Emotion wird die FeelingsButton-Komponente gerendered */}
                {positiveFeelings.map(positiveFeels => {
                    return(
                        <FeelingsButton
                            emotion={positiveFeels.emotion}
                            key ={positiveFeels.key}
                            id={positiveFeels.id}
                            points={positiveFeels.points}
                            onPress={isPressed => handleMood(positiveFeels.points, positiveFeels.emotion, isPressed)}

                        />
                    )
                })}

                {moderateFeelings.map(moderateFeels => {
                    return(
                        <FeelingsButton
                            emotion={moderateFeels.emotion}
                            key ={moderateFeels.key}
                            id={moderateFeels.id}
                            points={moderateFeels.points}
                            onPress={isPressed => handleMood(moderateFeels.points, moderateFeels.emotion, isPressed)}
                        />
                    )
                })}

                {negativeFeelings.map(negativeFeels => {
                    return(
                        <FeelingsButton
                            emotion={negativeFeels.emotion}
                            key ={negativeFeels.key}
                            id={negativeFeels.id}
                            points={negativeFeels.points}
                            onPress={isPressed => handleMood(negativeFeels.points, negativeFeels.emotion, isPressed)}
                        />
                    )
                })}

                {/* wurde die heutige Stimmung schon eingetragen -> Deaktivieren des Widgets */}
                <TouchableOpacity 
                    style={[styles.saveButton,disabled ? {backgroundColor: '#d3d3d3'} : {backgroundColor: 'white'}]} 
                    onPress={handleOnSave} 
                    disabled={disabled} 
                >
                    <Text style={[styles.buttonText, disabled ? {color: 'white', fontWeight: 'bold'} : {color: 'black', fontWeight: 'bold'}]} >
                        Speichern
                    </Text>   
                </TouchableOpacity>

            </View>
        
        </SafeAreaView>
    );
}


const styles = StyleSheet.create({

    screenView: {
        flex: 1,
        justifyContent: 'center',
        alignContent: 'center',
        paddingTop: Platform.OS === "android" ? StatusBar.currentHeight : 0

    },

    gradientBackground: {
        top: -12,
        left: -18,
        // width: 388,
        // height: 664,
        width: '200%',
        height: '200%',
        backgroundColor: "transparent",
        position: "absolute",
    },

    mainContentView: {
        flex: 1,
        paddingLeft: 10,
        paddingRight: 10,
        flexDirection: 'row',
        flexWrap: 'wrap',
        justifyContent: 'space-evenly',
        gap: 20
    },

    saveButton: {
        borderRadius: 5,
        // backgroundColor: 'white',
        width: 250,
        height: 50,
        justifyContent: 'center',
        alignItems: 'center', 
        marginTop: 50
       
    },

    buttonText: {
        fontSize: 16,
        fontWeight: 'bold',
        // color: 'whi',
    }
})

export default DailyFeelingsScreen;