import React, { useEffect, useState, useRef } from 'react';
import { View, Text, SafeAreaView, StatusBar, Platform } from 'react-native';

import { StyleSheet } from 'react-native';
import { LinearGradient } from "expo-linear-gradient";

import ExerciseDetailWidget from '../components/ExerciseDetailWidget';
import ExerciseDetailScreen from './ExerciseDetailScreen';

import { useSelector, useDispatch } from 'react-redux';
import { store } from '../redux/store';

import AsyncStorage from '@react-native-async-storage/async-storage';
import asyncStorageKeys from '../data/asyncStorageKeys';


function SocialExerciseScreen({ navigation }) {

    const socialExerciseData = require('../data/dailySocialExercise.json');
    const storeSocialActivity = store.getState().exerciseReducer.socialExercises
    const storeSocialActivity2 =useSelector(state => state.exerciseReducer.socialExercises)
    const [exercises, setExercises] = useState([])
    const [exerciseStorageData, setExerciseStorageData] = useState('')


    const renderDailyExercise = async () => {

        const exercise = {
            key: '', 
            index: '', 
            id: '',
            title: '', 
            description: '', 
            stackName: 'exercise_detail_screen',
            done: false
        }

      
        const currentDate = new Date().toLocaleDateString()

        // switch(JSON.stringify(currentDate)){
            switch(JSON.stringify('25.8.2023')){


            case JSON.stringify('25.8.2023'):
                exercise.key = socialExerciseData.socialDay1.socialExercise1_1.key, 
                exercise.index = socialExerciseData.socialDay1.socialExercise1_1.key, 
                exercise.id = socialExerciseData.socialDay1.socialExercise1_1.id,
                exercise.title = socialExerciseData.socialDay1.socialExercise1_1.title, 
                exercise.description = socialExerciseData.socialDay1.socialExercise1_1.description 
              
                break;
                
            case JSON.stringify('26.8.2023'):
                exercise.key = socialExerciseData.socialDay2.socialExercise2_1.key, 
                exercise.index = socialExerciseData.socialDay2.socialExercise2_1.key, 
                exercise.id = socialExerciseData.socialDay2.socialExercise2_1.id,
                exercise.title = socialExerciseData.socialDay2.socialExercise2_1.title, 
                exercise.description = socialExerciseData.socialDay2.socialExercise2_1.description 

               break;

            case JSON.stringify('27.8.2023'):
                exercise.key = socialExerciseData.socialDay3.socialExercise3_1.key, 
                exercise.index = socialExerciseData.socialDay3.socialExercise3_1.key, 
                exercise.id = socialExerciseData.socialDay3.socialExercise3_1.id,
                exercise.title = socialExerciseData.socialDay3.socialExercise3_1.title, 
                exercise.description = socialExerciseData.socialDay3.socialExercise3_1.description 

                break;

            case JSON.stringify('28.8.2023'):
                exercise.key = socialExerciseData.socialDay4.socialExercise4_1.key, 
                exercise.index = socialExerciseData.socialDay4.socialExercise4_1.key, 
                exercise.id = socialExerciseData.socialDay4.socialExercise4_1.id,
                exercise.title = socialExerciseData.socialDay4.socialExercise4_1.title, 
                exercise.description = socialExerciseData.socialDay4.socialExercise4_1.description 

               break;

            case JSON.stringify('29.8.2023'):
                exercise.key = socialExerciseData.socialDay5.socialExercise5_1.key, 
                exercise.index = socialExerciseData.socialDay5.socialExercise5_1.key, 
                exercise.id = socialExerciseData.socialDay5.socialExercise5_1.id,
                exercise.title = socialExerciseData.socialDay5.socialExercise5_1.title, 
                exercise.description = socialExerciseData.socialDay5.socialExercise5_1.description 

              break;

            default: 
                exercise.key = socialExerciseData.socialDay1.socialExercise1_1.key, 
                exercise.index = socialExerciseData.socialDay1.socialExercise1_1.key, 
                exercise.id = socialExerciseData.socialDay1.socialExercise1_1.id,
                exercise.title = socialExerciseData.socialDay1.socialExercise1_1.title, 
                exercise.description = socialExerciseData.socialDay1.socialExercise1_1.description 
            
                break;
         }  
       
        let newMainContent = [...exercises]
        let data = {...newMainContent[0]}
        data = exercise
        newMainContent[0] = data

     
        setExercises(newMainContent)
        

    }

    const defineDate = async() => {
        const currentDate = new Date().toLocaleDateString()

        if(JSON.stringify(currentDate) == JSON.stringify('24.8.2023')){
            setExerciseStorageData(asyncStorageKeys.socialExercises_day1_storage)

            
        }else if(JSON.stringify(currentDate) == JSON.stringify('25.8.2023')){
            setExerciseStorageData(asyncStorageKeys.socialExercises_day1_storage)

        }else if(JSON.stringify(currentDate) == JSON.stringify('26.8.2023')){
           setExerciseStorageData(asyncStorageKeys.socialExercises_day2_storage)

        }else if(JSON.stringify(currentDate) == JSON.stringify('27.8.2023')){
            setExerciseStorageData(asyncStorageKeys.socialExercises_day3_storage)
 
        }else if(JSON.stringify(currentDate) == JSON.stringify('28.8.2023')){
            setExerciseStorageData(asyncStorageKeys.socialExercises_day4_storage)
 
        }else if(JSON.stringify(currentDate) == JSON.stringify('29.8.2023')){
            setExerciseStorageData(asyncStorageKeys.socialExercises_day5_storage)
 
        }else {
            setExerciseStorageData(asyncStorageKeys.socialExercises_day1_storage)
 
        } 
    }

    const storeDataToStorage = async () => {
        defineDate()

        let exercise
        let exerciseToStorage = []

        storeSocialActivity2.map((currentExercise) => {
            exercise = {
                "day" : new Date().toLocaleDateString(),
                "socialExercises" : currentExercise.title,
                "done": true
                
            }
            exerciseToStorage.push(exercise)
        })

        try{
               
            const data = await AsyncStorage.setItem(JSON.stringify(exerciseStorageData), JSON.stringify(exerciseToStorage))

        }catch(e){
            console.log(e)
        }
    }


   
    useEffect(() => {
        storeDataToStorage()
    }, [storeSocialActivity2])

    useEffect(() => {
        renderDailyExercise()
    }, [])
    

    return (
        <SafeAreaView style={styles.screenView}>

            <LinearGradient
                style={styles.gradientBackground}
                locations={[0, 0.99, 0.99]}
                colors={["#e28282", "rgba(243, 162, 57, 0.42)", "rgba(255, 185, 4, 0)"]}
            />

            <View style={styles.mainContentView}>

                {exercises.map(exercise => {
                    let currentData;
                    if(storeSocialActivity.length !== 0 ){
                        console.log("SocialExerciseScreen: \n StoreActivity array is not empty. ")
                        currentData = storeSocialActivity.find(exercise2 => exercise2.id === exercise.id)
                            if(currentData) {
                                console.log("Match found! " + JSON.stringify(currentData))
                            }else{
                                console.log("No match found!")
                            }
                    }else{
                        console.log("SocialExerciseScreen: \n StoreActivity array still empty so far.")
                    }     

                    return(
                        <ExerciseDetailWidget
                            key = {exercise.index}
                            index = {exercise.index}
                            id = {exercise.id}
                            title = {exercise.title} 
                            description={exercise.description}
                            stackName = {exercise.stackName}
                            done={!currentData ? exercise.done : currentData.done}
                        />
                    )
                })}

            </View>

        </SafeAreaView>
    );
}

const styles = StyleSheet.create({

    screenView: {
        flex: 1,
        justifyContent: 'center',
        alignContent: 'center',
        paddingTop: Platform.OS === "android" ? StatusBar.currentHeight : 0
    
    },

    gradientBackground: {
        top: -12,
        left: -18,
        // width: 388,
        // height: 664,
        width: '200%',
        height: '200%',
        backgroundColor: "transparent",
        position: "absolute",
    },

    mainContentView: {
        flex: 1,
        justifyContent: 'flex-start',
        alignItems: 'center',
    },

    localNotification: {
        backgroundColor: 'white',
        borderRadius: 5,
        width: 300,
        height: 40,
        justifyContent: 'flex-start',
        alignContent: 'flex-start'
        
    },

    localNotificationText: {
        fontSize: 14,
        fontWeight: 'bold',
        paddingLeft: '5%'
    }

})

export default SocialExerciseScreen;