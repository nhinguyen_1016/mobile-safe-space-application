import React, { useEffect, useState, useRef } from 'react';
import { View, Text, SafeAreaView, StatusBar, Platform } from 'react-native';

import { StyleSheet } from 'react-native';
import { LinearGradient } from "expo-linear-gradient";

import ExerciseDetailWidget from '../components/ExerciseDetailWidget';

import { useSelector, useDispatch } from 'react-redux';
import AsyncStorage from '@react-native-async-storage/async-storage';
import asyncStorageKeys from '../data/asyncStorageKeys';

function PhysicalExerciseScreen() {

    const physicalExerciseData = require('../data/dailyPhysicalExercise.json');
    const storePhysicalActivity = useSelector(state => state.exerciseReducer.physicalExercises)
    const [exerciseStorageData, setExerciseStorageData] = useState('')

    const [exercises, setExercises] = useState([])

    const renderDailyExercise = async () => {

        const exercise = {
            key: '', 
            index: '', 
            id: '',
            title: '', 
            description: '', 
            stackName: 'exercise_detail_screen',
            done: false
        }

        const currentDate = new Date().toLocaleDateString()

        //für Testzwecke außerhalb des Playtestings wird ein vordefinierter Tag benutzt
        //ansonsten wird der aktuelle Tag benutzt:
        // switch(JSON.stringify(currentDate)){

            switch(JSON.stringify('25.8.2023')){

            case JSON.stringify('25.8.2023'):
                exercise.key = physicalExerciseData.physicalDay1.physicalExercise1_1.key, 
                exercise.index = physicalExerciseData.physicalDay1.physicalExercise1_1.key, 
                exercise.id = physicalExerciseData.physicalDay1.physicalExercise1_1.id,
                exercise.title = physicalExerciseData.physicalDay1.physicalExercise1_1.title, 
                exercise.description = physicalExerciseData.physicalDay1.physicalExercise1_1.description 
               
                break;
                
            case JSON.stringify('26.8.2023'):
                exercise.key = physicalExerciseData.physicalDay2.physicalExercise2_1.key, 
                exercise.index = physicalExerciseData.physicalDay2.physicalExercise2_1.key, 
                exercise.id = physicalExerciseData.physicalDay2.physicalExercise2_1.id,
                exercise.title = physicalExerciseData.physicalDay2.physicalExercise2_1.title, 
                exercise.description = physicalExerciseData.physicalDay2.physicalExercise2_1.description 

                break;

            case JSON.stringify('27.8.2023'):
                exercise.key = physicalExerciseData.physicalDay3.physicalExercise3_1.key, 
                exercise.index = physicalExerciseData.physicalDay3.physicalExercise3_1.key, 
                exercise.id = physicalExerciseData.physicalDay3.physicalExercise3_1.id,
                exercise.title = physicalExerciseData.physicalDay3.physicalExercise3_1.title, 
                exercise.description = physicalExerciseData.physicalDay3.physicalExercise3_1.description 

              break;

            case JSON.stringify('28.8.2023'):
                exercise.key = physicalExerciseData.physicalDay4.physicalExercise4_1.key, 
                exercise.index = physicalExerciseData.physicalDay4.physicalExercise4_1.key, 
                exercise.id = physicalExerciseData.physicalDay4.physicalExercise4_1.id,
                exercise.title = physicalExerciseData.physicalDay4.physicalExercise4_1.title, 
                exercise.description = physicalExerciseData.physicalDay4.physicalExercise4_1.description 

              break;

            case JSON.stringify('29.8.2023'):
                exercise.key = physicalExerciseData.physicalDay5.physicalExercise5_1.key, 
                exercise.index = physicalExerciseData.physicalDay5.physicalExercise5_1.key, 
                exercise.id = physicalExerciseData.physicalDay5.physicalExercise5_1.id,
                exercise.title = physicalExerciseData.physicalDay5.physicalExercise5_1.title, 
                exercise.description = physicalExerciseData.physicalDay5.physicalExercise5_1.description 

                break;

            default: 
            exercise.key = physicalExerciseData.physicalDay1.physicalExercise1_1.key, 
            exercise.index = physicalExerciseData.physicalDay1.physicalExercise1_1.key, 
            exercise.id = physicalExerciseData.physicalDay1.physicalExercise1_1.id,
            exercise.title = physicalExerciseData.physicalDay1.physicalExercise1_1.title, 
            exercise.description = physicalExerciseData.physicalDay1.physicalExercise1_1.description 
           
            break;
         }  
       
        let newMainContent = [...exercises]
        let data = {...newMainContent[0]}
        data = exercise
        newMainContent[0] = data

        setExercises(newMainContent)
        
    }

    
    const defineDate = async() => {
        
        const currentDate = new Date().toLocaleDateString()

        if(JSON.stringify(currentDate) == JSON.stringify('24.8.2023')){
            setExerciseStorageData(asyncStorageKeys.physicalExercises_day1_storage)

            
        }else if(JSON.stringify(currentDate) == JSON.stringify('25.8.2023')){
            setExerciseStorageData(asyncStorageKeys.physicalExercises_day1_storage)

        }else if(JSON.stringify(currentDate) == JSON.stringify('26.8.2023')){
           setExerciseStorageData(asyncStorageKeys.physicalExercises_day2_storage)

        }else if(JSON.stringify(currentDate) == JSON.stringify('27.8.2023')){
            setExerciseStorageData(asyncStorageKeys.physicalExercises_day3_storage)
 
        }else if(JSON.stringify(currentDate) == JSON.stringify('28.8.2023')){
            setExerciseStorageData(asyncStorageKeys.physicalExercises_day4_storage)
 
        }else if(JSON.stringify(currentDate) == JSON.stringify('29.8.2023')){
            setExerciseStorageData(asyncStorageKeys.physicalExercises_day5_storage)
 
            //default für Zeiten außerhalb des Playtestings
        } else {
        setExerciseStorageData(asyncStorageKeys.physicalExercises_day1_storage)
        } 
    }
    

    //Funktion, um abgeschlossene Aktivität einen bestimmten Tag zuzuweisen -> wichtig für die Timeline 
    const storeDataToStorage = async () => {
        defineDate()
        let exercise
        let exerciseToStorage = []

        storePhysicalActivity.map((currentExercise) => {
            exercise = {
                "day" : new Date().toLocaleDateString(),
                "physicalExercises" : currentExercise.title,
                "done": true
                
            }
            exerciseToStorage.push(exercise)
        })

        try{
               
            console.log("PhysicalScreen: Async key is: " + JSON.stringify(exerciseStorageData) )
            await AsyncStorage.setItem(JSON.stringify(exerciseStorageData), JSON.stringify(exerciseToStorage))
            console.log("PhysicalScreen: Saved exercise to storage!")
        
        }catch(e){
            console.log(e)
            console.log("PhysicalScreen: Save exercise to storage didn't work.")
        }

        try{
             const data2 = await AsyncStorage.getItem(JSON.stringify(exerciseStorageData))
            JSON.parse(data2)
            console.log("PhysicaExerciseScreen: Successfully got physical exercises to AsyncStorage! \n Data is: " + data2 )

        }catch(e){
            console.log(e)
        }


    }


    useEffect(() => {
        renderDailyExercise()
    }, [])

    useEffect(() => {
        storeDataToStorage()
    }, [storePhysicalActivity])
 
    return (
        <SafeAreaView style={styles.screenView}>

            <LinearGradient
                style={styles.gradientBackground}
                locations={[0, 0.99, 0.99]}
                colors={["#e28282", "rgba(243, 162, 57, 0.42)", "rgba(255, 185, 4, 0)"]}
            />

            <View style={styles.mainContentView}>

                {exercises.map(exercise => {
                    let currentData;

                    //Vergleich zwischen Aktivitäten aus dem Store und dem State
                    //identisch -> Aktivität schon abgeschlossen!
                    //Abgeschlossen Button wird für diese Aktivität unten deaktiviert
                    if(storePhysicalActivity.length !== 0 ){
                        console.log("PhysicalExerciseScreen: \n StoreActivity array is not empty. ")
                        currentData = storePhysicalActivity.find(exercise2 => exercise2.id === exercise.id)
                            if(currentData) {
                                console.log("Match found! " + JSON.stringify(currentData))
                            }else{
                                console.log("No match found!")
                            }
                    }else{
                        console.log("PhysicalExerciseScreen: \n StoreActivity array still empty so far.")
                    }     

                    return(
                        <ExerciseDetailWidget
                            key = {exercise.index}
                            index = {exercise.index}
                            id = {exercise.id}
                            title = {exercise.title} 
                            description={exercise.description}
                            stackName = {exercise.stackName}
                            done={!currentData ? exercise.done : currentData.done}
                        />
                    )
                })}

            </View>
        </SafeAreaView>
    );
}

const styles = StyleSheet.create({

    screenView: {
        flex: 1,
        justifyContent: 'center',
        alignContent: 'center',
        paddingTop: Platform.OS === "android" ? StatusBar.currentHeight : 0
    
    },

    gradientBackground: {
        top: -12,
        left: -18,
        // width: 388,
        // height: 664,
        width: '200%',
        height: '200%',
        backgroundColor: "transparent",
        position: "absolute",
    },

    mainContentView: {
        flex: 1,
        justifyContent: 'flex-start',
        alignItems: 'center',
    },

    localNotification: {
        backgroundColor: 'white',
        borderRadius: 5,
        width: 300,
        height: 40,
        justifyContent: 'flex-start',
        alignContent: 'flex-start'
        
    },

    localNotificationText: {
        fontSize: 14,
        fontWeight: 'bold',
        paddingLeft: '5%'
    }

})

export default PhysicalExerciseScreen;