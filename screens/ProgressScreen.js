import React, { useEffect, useState } from 'react';
import { View, Text, SafeAreaView, Platform, StatusBar} from 'react-native';
import Timeline from 'react-native-timeline-flatlist'

import {store} from '../redux/store';
import { useSelector } from 'react-redux';

import { StyleSheet } from 'react-native';

import AsyncStorage from '@react-native-async-storage/async-storage';
import asyncStorageKeys from '../data/asyncStorageKeys';



function ProgressScreen() {

    //useSelector-Hook -> Laden der Objekte aus dem Store, um diese später den useEffectHooks als Dependency zuzuweisen
    // -> dient dazu, die Timeline bei Änderungen in dem Store erneut zu rendern (z.B bei Hinzufügen einer neuen Aktivität)
    const storeMood = useSelector(state => state.moodReducer.mood)
    const storePhysicalExercises = useSelector(state => state.exerciseReducer.physicalExercises)
    const storeMentalExercises = useSelector(state => state.exerciseReducer.mentalExercises)
    const storeSocialExercises = useSelector(state => state.exerciseReducer.socialExercises)
    const storeCustomActivities = useSelector(state => state.activityReducer.doneActivities)
    const storeBadges = useSelector(state => state.badgeReducer.badges)

    //Physische Aktivitäten der einzelnen Tage 
    const [physicalExerciseNames, setPhysicalExerciseNames] = useState([])
    const [physicalExerciseNames2, setPhysicalExerciseNames2] = useState([])
    const [physicalExerciseNames3, setPhysicalExerciseNames3] = useState([])
    const [physicalExerciseNames4, setPhysicalExerciseNames4] = useState([])
    const [physicalExerciseNames5, setPhysicalExerciseNames5] = useState([])
    const [physicalExerciseNames6, setPhysicalExerciseNames6] = useState([])

    //Mentale Aktivitäten der einzelnen Tage
    const [mentalExerciseNames, setMentalExerciseNames] = useState([])
    const [mentalExerciseNames2, setMentalExerciseNames2] = useState([])
    const [mentalExerciseNames3, setMentalExerciseNames3] = useState([])
    const [mentalExerciseNames4, setMentalExerciseNames4] = useState([])
    const [mentalExerciseNames5, setMentalExerciseNames5] = useState([])
    const [mentalExerciseNames6, setMentalExerciseNames6] = useState([])

    //Soziale Aktivitäten der einzelnen Tage
    const [socialExerciseNames, setSocialExerciseNames] = useState([])
    const [socialExerciseNames2, setSocialExerciseNames2] = useState([])
    const [socialExerciseNames3, setSocialExerciseNames3] = useState([])
    const [socialExerciseNames4, setSocialExerciseNames4] = useState([])
    const [socialExerciseNames5, setSocialExerciseNames5] = useState([])
    const [socialExerciseNames6, setSocialExerciseNames6] = useState([])

    //Custom Aktivitäten der einzelnen Tage
    const [customActivitiesNames, setCustomActivitiesNames] = useState([])
    const [customActivitiesNames2, setCustomActivitiesNames2] = useState([])
    const [customActivitiesNames3, setCustomActivitiesNames3] = useState([])
    const [customActivitiesNames4, setCustomActivitiesNames4] = useState([])
    const [customActivitiesNames5, setCustomActivitiesNames5] = useState([])
    const [customActivitiesNames6, setCustomActivitiesNames6] = useState([])

    //Stimmung der einzelnen Tage
    const [moodName, setMoodName] = useState([])
    const [moodName2, setMoodName2] = useState([])
    const [moodName3, setMoodName3] = useState([])
    const [moodName4, setMoodName4] = useState([])
    const [moodName5, setMoodName5] = useState([])
    const [moodName6, setMoodName6] = useState([])

    const moodString = "Stimmung: \n" + moodName + '\n'
    const moodString2 = "Stimmung: \n" + moodName2 + '\n'
    const moodString3 = "Stimmung: \n" + moodName3 + '\n'
    const moodString4 = "Stimmung: \n" + moodName4 + '\n'
    const moodString5 = "Stimmung: \n" + moodName5 + '\n'
    const moodString6  = "Stimmung: \n" + moodName6 + '\n'


    const doneExercises = '\n' + physicalExerciseNames + '\n' + mentalExerciseNames + '\n' + socialExerciseNames + '\n' + customActivitiesNames + '\n'

    const doneExercises2 = '\n' + physicalExerciseNames2 + '\n' + mentalExerciseNames2 + '\n' + socialExerciseNames2 + '\n' + customActivitiesNames2 + '\n'
    const doneExercises3 = '\n' + physicalExerciseNames3 + '\n' + mentalExerciseNames3 + '\n' + socialExerciseNames3 + '\n' + customActivitiesNames3 + '\n'
    const doneExercises4 = '\n' + physicalExerciseNames4 + '\n' + mentalExerciseNames4 + '\n' + socialExerciseNames4 + '\n' + customActivitiesNames4 + '\n'
    const doneExercises5 = '\n' + physicalExerciseNames5 + '\n' + mentalExerciseNames5 + '\n' + socialExerciseNames5 + '\n' + customActivitiesNames5 + '\n'
    const doneExercises6 = '\n' + physicalExerciseNames6 + '\n' + mentalExerciseNames6 + '\n' + socialExerciseNames6 + '\n' + customActivitiesNames6 + '\n'


    const exerciseString = "Erledigte Aktivitäten: \n" + doneExercises + '\n'
    const exerciseString2 = "Erledigte Aktivitäten: \n" + doneExercises2 + '\n'
    const exerciseString3 = "Erledigte Aktivitäten: \n" + doneExercises3 + '\n'
    const exerciseString4 = "Erledigte Aktivitäten: \n" + doneExercises4 + '\n'
    const exerciseString5 = "Erledigte Aktivitäten: \n" + doneExercises5 + '\n'
    const exerciseString6 = "Erledigte Aktivitäten: \n" + doneExercises6 + '\n'


    //Strings aller Daten für jeden einzelnen Tag
    const dayDescription1 = moodString + exerciseString 
    const dayDescription2 = moodString2 + exerciseString2 
    const dayDescription3 = moodString3 + exerciseString3 
    const dayDescription4 = moodString4 + exerciseString4 
    const dayDescription5 = moodString5 + exerciseString5 
    const dayDescription6 = moodString6 + exerciseString6 


    const data = [
        {time: '25.08.23', title: '', description: dayDescription1},
        {time: '26.08.23', title: '', description: dayDescription2},
        {time: '27.08.23', title: '', description: dayDescription3},
        {time: '28.08.23', title: '', description: dayDescription4},
        {time: '29.08.23', title: '', description: dayDescription5},
        
    ]

    //ladet alle Einträge der täglichen Stimmung
    //falls ein Eintrag leer ist -> leerer String
    //falls ein Eintrag gefunden wurde -> Zuweisen an Strings zu dem State, um gerendered zu werden
    const getStorageMood = async () => {
        let moodDay1 = await AsyncStorage.getItem(asyncStorageKeys.mood_day1_storage)
        if(!moodDay1) {
            console.log("ProgressScreen: No mood for day1.")
        }else{
            console.log("ProgressScreen: mood for day1.")
            moodDay1 = JSON.parse(moodDay1)
            setMoodName(moodDay1.mood)
        }

        
        let moodDay2 = await AsyncStorage.getItem(asyncStorageKeys.mood_day2_storage)
        if(!moodDay2) {
                 console.log("ProgressScreen: No mood for day2.")
        }else{
            console.log("ProgressScreen: mood for day2.")
            moodDay2 = JSON.parse(moodDay2)
            setMoodName2(moodDay2.mood)
        }

                
        let moodDay3 = await AsyncStorage.getItem(asyncStorageKeys.mood_day3_storage)
        if(!moodDay3) {
            console.log("ProgressScreen: No mood for day3.")
        }else{
            console.log("ProgressScreen: mood for day3.")

            moodDay3 = JSON.parse(moodDay3)
            setMoodName3(moodDay3.mood)
        }

        let moodDay4 = await AsyncStorage.getItem(asyncStorageKeys.mood_day4_storage)
        if(!moodDay4) {
            console.log("ProgressScreen: No mood for day4.")
        }else{
            console.log("ProgressScreen: No mood for day4.")
            moodDay4 = JSON.parse(moodDay4)
            setMoodName4(moodDay4.mood)
        }

                
        let moodDay5 = await AsyncStorage.getItem(asyncStorageKeys.mood_day5_storage)
        if(!moodDay5) {
            console.log("ProgressScreen: No mood for day5.")

        }else{
            console.log("ProgressScreen:  mood for day5.")

            moodDay5 = JSON.parse(moodDay5)
            setMoodName5(moodDay5.mood)
        }

            
        let moodDay6 = await AsyncStorage.getItem(asyncStorageKeys.mood_day6_storage)
        if(!moodDay6) {
            console.log("ProgressScreen: No mood for day6.")

        }else{
            console.log("ProgressScreen: mood for day6.")

            moodDay6 = JSON.parse(moodDay6)
            setMoodName6(moodDay6.mood)
        }      
    }


    const getStoragePhysicalExercises = async () => {    

        //da der Schlüssel für einen Eintrag in den AsyncStorage nur als String möglich ist 
        // -> JSON.stringify parst den Schlüssel in einen String um 
        let physicalDay1 = await AsyncStorage.getItem(JSON.stringify(asyncStorageKeys.physicalExercises_day1_storage))
        if(!physicalDay1) {
            console.log("ProgressScreen: No physical exercises registered for day1.")
    
        }else{
            console.log("ProgressScreen: Physical exercises registered for day1.")
            physicalDay1 = JSON.parse(physicalDay1)
            let data = []
                physicalDay1.map((exercise) => {
                data.push(exercise.physicalExercises + '\n')
            })                    
            
            setPhysicalExerciseNames(data)
        }

        const storageKey2 = JSON.stringify(asyncStorageKeys.physicalExercises_day1_storage)
        console.log("ProgressScreen: " + storageKey2)
        let physicalDay2 = await AsyncStorage.getItem(JSON.stringify(asyncStorageKeys.physicalExercises_day2_storage))
        if(!physicalDay2) {
            console.log("ProgressScreen: No physical exercises registered for day2.")

        }else{
            console.log("ProgressScreen: Physical exercises registered for day2.")

            physicalDay2 = JSON.parse(physicalDay2)

            let data2 = []
            physicalDay2.map((exercise) => {
                data2.push(exercise.physicalExercises + '\n')
            })                    
        
            setPhysicalExerciseNames2(data2)
        }
            

        let physicalDay3 = await AsyncStorage.getItem(JSON.stringify(asyncStorageKeys.physicalExercises_day3_storage))
        if(!physicalDay3) {
            console.log("ProgressScreen: No physical exercises registered for day3.")

        }else{
            console.log("ProgressScreen: Physical exercises registered for day3.")
            physicalDay3 = JSON.parse(physicalDay3)
            let data3 = []
            physicalDay3.map((exercise) => {
                data3.push(exercise.physicalExercises + '\n')

            })                    
            setPhysicalExerciseNames3(data3)
        }

        let physicalDay4 = await AsyncStorage.getItem(JSON.stringify(asyncStorageKeys.physicalExercises_day4_storage))
        if(!physicalDay4) {
            // console.log("ProgressScreen: No physical exercises registered for day4.")

        }else{
            // console.log("ProgressScreen: Physical exercises registered for day4.")
            physicalDay4 = JSON.parse(physicalDay4)
            let data4 = []
            physicalDay4.map((exercise) => {
                data4.push(exercise.physicalExercises + '\n')
            })                    
        
            setPhysicalExerciseNames4(data)
        }

        let physicalDay5 = await AsyncStorage.getItem(JSON.stringify(asyncStorageKeys.physicalExercises_day5_storage))
        if(!physicalDay5) {
            // console.log("ProgressScreen: No physical exercises registered for day5.")

        }else{
            // console.log("ProgressScreen: Physical exercises registered for day5.")
            physicalDay5 = JSON.parse(physicalDay5)
            let data5 = []
            physicalDay5.map((exercise) => {
                data5.push(exercise.physicalExercises + '\n')
            })                    
        
            setPhysicalExerciseNames5(data)
        }

        let physicalDay6 = await AsyncStorage.getItem(JSON.stringify(asyncStorageKeys.physicalExercises_day6_storage))
        if(!physicalDay6) {
            // console.log("ProgressScreen: No physical exercises registered for day6.")

        }else{
            // console.log("ProgressScreen: Physical exercises registered for day6.")
            physicalDay6 = JSON.parse(physicalDay6)
            let data6 = []
            physicalDay6.map((exercise) => {
                data6.push(exercise.physicalExercises + '\n')
            })                    
        
            setPhysicalExerciseNames6(data)
        }
    }


    const getStorageMentalExercises = async () => {    


        let mentalDay1 = await AsyncStorage.getItem(JSON.stringify(asyncStorageKeys.mentalExercises_day1_storage))
        if(!mentalDay1) {
            console.log("ProgressScreen: No mental exercises registered for day1.")

        }else{
            console.log("ProgressScreen: Mental exercises registered for day1.")
            mentalDay1 = JSON.parse(mentalDay1)
            let data = []
                mentalDay1.map((exercise) => {
                data.push(exercise.mentalExercises + '\n')
            })                    
            
            setMentalExerciseNames(data)
            console.log("ProgressScreen: mentalExercises1: " + JSON.stringify(data1))

        }

        let mentalDay2 = await AsyncStorage.getItem(JSON.stringify(asyncStorageKeys.mentalExercises_day2_storage))
        if(!mentalDay2) {
            console.log("ProgressScreen: No mental exercises registered for day2.")

        }else{
            console.log("ProgressScreen: mental exercises registered for day2.")
            mentalDay2 = JSON.parse(mentalDay2)
            let data2 = []
                mentalDay2.map((exercise) => {
                data2.push(exercise.mentalExercises + '\n')
            })                    
            
            setMentalExerciseNames2(data2)
            console.log("ProgressScreen: mentalExercises2: " + JSON.stringify(data2))
        }

        let mentalDay3 = await AsyncStorage.getItem(JSON.stringify(asyncStorageKeys.mentalExercises_day3_storage))
        if(!mentalDay3) {
            // console.log("ProgressScreen: No mental exercises registered for day1.")

        }else{
            // console.log("ProgressScreen: mental exercises registered for day1.")
            mentalDay3 = JSON.parse(mentalDay3)
            let data3 = []
                mentalDay3.map((exercise) => {
                data3.push(exercise.mentalExercises + '\n')
            })                    
            
            setMentalExerciseNames3(data3)
        }

        let mentalDay4 = await AsyncStorage.getItem(JSON.stringify(asyncStorageKeys.mentalExercises_day4_storage))
        if(!mentalDay4) {
            // console.log("ProgressScreen: No mental exercises registered for day1.")

        }else{
            // console.log("ProgressScreen: mental exercises registered for day1.")
            mentalDay4 = JSON.parse(mentalDay4)
            let data4 = []
                mentalDay4.map((exercise) => {
                data4.push(exercise.mentalExercises + '\n')
            })                    
            
            setMentalExerciseNames4(data4)
        }

        let mentalDay5 = await AsyncStorage.getItem(JSON.stringify(asyncStorageKeys.mentalExercises_day5_storage))
        if(!mentalDay5) {
            // console.log("ProgressScreen: No mental exercises registered for day1.")

        }else{
            // console.log("ProgressScreen: mental exercises registered for day1.")
            mentalDay5 = JSON.parse(mentalDay5)
            let data5 = []
                mentalDay5.map((exercise) => {
                data5.push(exercise.mentalExercises + '\n')
            })                    
            
            setMentalExerciseNames5(data5)
        }

        let mentalDay6 = await AsyncStorage.getItem(JSON.stringify(asyncStorageKeys.mentalExercises_day6_storage))
        if(!mentalDay6) {
            // console.log("ProgressScreen: No mental exercises registered for day1.")

        }else{
            // console.log("ProgressScreen: mental exercises registered for day1.")
            mentalDay6 = JSON.parse(mentalDay6)
            let data6 = []
                mentalDay6.map((exercise) => {
                data6.push(exercise.mentalExercises + '\n')
            })                    
            
            setMentalExerciseNames6(data6)
        }

     
    }

    const getStorageSocialExercises = async () => {    

        let socialDay1 = await AsyncStorage.getItem(JSON.stringify(asyncStorageKeys.socialExercises_day1_storage))
        if(!socialDay1) {
            // console.log("ProgressScreen: No social exercises registered for day1.")

        }else{
            // console.log("ProgressScreen: social exercises registered for day1.")
            socialDay1 = JSON.parse(socialDay1)
            let data = []
            socialDay1.map((exercise) => {
                data.push(exercise.socialExercises + '\n')
            })                    
            
            setSocialExerciseNames(data)
        }

        let socialDay2 = await AsyncStorage.getItem(JSON.stringify(asyncStorageKeys.socialExercises_day2_storage))
        if(!socialDay2) {
            // console.log("ProgressScreen: Social exercises registered for day2.")

        }else{
            // console.log("ProgressScreen: Social exercises registered for day2.")
            socialDay2 = JSON.parse(socialDay2)
            let data2 = []
            socialDay2.map((exercise) => {
                data2.push(exercise.socialExercises + '\n')
            })                    
            
            setSocialExerciseNames2(data2)
        }

        let socialDay3 = await AsyncStorage.getItem(JSON.stringify(asyncStorageKeys.socialExercises_day3_storage))
        if(!socialDay3) {
            // console.log("ProgressScreen: No social exercises registered for day1.")

        }else{
            // console.log("ProgressScreen: social exercises registered for day1.")
            socialDay3 = JSON.parse(socialDay3)
            let data3 = []
                socialDay3.map((exercise) => {
                data3.push(exercise.socialExercises + '\n')
            })                    
            
            setSocialExerciseNames3(data3)
        }

            
        let socialDay4 = await AsyncStorage.getItem(JSON.stringify(asyncStorageKeys.socialExercises_day4_storage))
        if(!socialDay4) {
            // console.log("ProgressScreen: No social exercises registered for day1.")

        }else{
            // console.log("ProgressScreen: social exercises registered for day1.")
            socialDay4 = JSON.parse(socialDay4)
            let data4 = []
                socialDay4.map((exercise) => {
                data4.push(exercise.socialExercises + '\n')
            })                    
            
            setSocialExerciseNames4(data4)
        }
            

        let socialDay5 = await AsyncStorage.getItem(JSON.stringify(asyncStorageKeys.socialExercises_day5_storage))
        if(!socialDay5) {
            // console.log("ProgressScreen: No social exercises registered for day1.")

        }else{
            // console.log("ProgressScreen: social exercises registered for day1.")
            socialDay5 = JSON.parse(socialDay5)
            let data5 = []
                socialDay5.map((exercise) => {
                data5.push(exercise.socialExercises + '\n')
            })                    
            
            setSocialExerciseNames5(data5)
        }

        let socialDay6 = await AsyncStorage.getItem(JSON.stringify(asyncStorageKeys.socialExercises_day6_storage))
        if(!socialDay6) {
            // console.log("ProgressScreen: No social exercises registered for day1.")

        }else{
            // console.log("ProgressScreen: social exercises registered for day1.")
            socialDay6 = JSON.parse(socialDay6)
            let data6 = []
                socialDay6.map((exercise) => {
                data6.push(exercise.socialExercises + '\n')
            })                    
            
            setSocialExerciseNames6(data6)
        }

    }

    const getStorageCustomExercises = async () => {    

        let customDay1 = await AsyncStorage.getItem(JSON.stringify(asyncStorageKeys.customExercises_day1_storage))
        if(!customDay1) {
            // console.log("ProgressScreen: No physical exercises registered for day1.")
        }else{
            // console.log("ProgressScreen: Physical exercises registered for day1.")
            customDay1 = JSON.parse(customDay1)
            let data = []
            customDay1.map((exercise) => {
                data.push(exercise.customExercises + '\n')
            })                    
            
            setCustomActivitiesNames(data)
        }


        let customDay2 = await AsyncStorage.getItem(JSON.stringify(asyncStorageKeys.customExercises_day2_storage))
        if(!customDay2) {
            // console.log("ProgressScreen: No physical exercises registered for day1.")

        }else{
            // console.log("ProgressScreen: Physical exercises registered for day1.")
            customDay2 = JSON.parse(customDay2)
            let data2 = []
            customDay2.map((exercise) => {
                data2.push(exercise.customExercises + '\n')
            })   

            setCustomActivitiesNames2(data2)

        }
                 
            

        let customDay3 = await AsyncStorage.getItem(JSON.stringify(asyncStorageKeys.customExercises_day3_storage))
        if(!customDay3) {
                // console.log("ProgressScreen: No physical exercises registered for day1.")
    
        }else{
                // console.log("ProgressScreen: Physical exercises registered for day1.")
            customDay3 = JSON.parse(customDay3)
            let data3 = []
            customDay3.map((exercise) => {
                data3.push(exercise.customExercises + '\n')
            })                    
   
            setCustomActivitiesNames3(data3)
       
        }

        
        let customDay4 = await AsyncStorage.getItem(JSON.stringify(asyncStorageKeys.customExercises_day4_storage))
        if(!customDay4) {
            // console.log("ProgressScreen: No physical exercises registered for day1.")

        }else{
            // console.log("ProgressScreen: Physical exercises registered for day1.")
            customDay4 = JSON.parse(customDay4)
            let data4 = []
            customDay4.map((exercise) => {
                data4.push(exercise.customExercises + '\n')
            })                    
            
            setCustomActivitiesNames4(data4)
        }

            
        let customDay5 = await AsyncStorage.getItem(JSON.stringify(asyncStorageKeys.customExercises_day5_storage))
        if(!customDay5) {
            // console.log("ProgressScreen: No physical exercises registered for day1.")

        }else{
            // console.log("ProgressScreen: Physical exercises registered for day1.")
            customDay5 = JSON.parse(customDay5)
            let data5 = []
            customDay5.map((exercise) => {
                data5.push(exercise.customExercises + '\n')
            })                    
            
            setCustomActivitiesNames5(data5)
        }

        let customDay6 = await AsyncStorage.getItem(JSON.stringify(asyncStorageKeys.customExercises_day6_storage))
        if(!customDay6) {
            // console.log("ProgressScreen: No physical exercises registered for day1.")

        }else{
            // console.log("ProgressScreen: Physical exercises registered for day1.")
            customDay6 = JSON.parse(customDay6)
            let data6 = []
            customDay6.map((exercise) => {
                data6.push(exercise.customExercises + '\n')
            })                    
            
            setCustomActivitiesNames6(data6)
        }
    }

    //lädt tägliche Stimmung aus dem Store und rendert ProgressScreen, sobald sich etwas an der Stimmung in dem Store ändert 
    useEffect(() => {
        console.log("ProgressScreen: Store mood state changed.")
        getStorageMood()
    }, [storeMood])


    //lädt Aktivitäten aller bisherigen Tage und rendert ProgressScreen, sobald eine neue Aktivität den physischen Aktivitäten im Store hinzugefügt wurde
    useEffect(() => {
        getStoragePhysicalExercises()

    }, [storePhysicalExercises])

        //lädt Aktivitäten aller bisherigen Tage und rendert ProgressScreen, sobald eine neue Aktivität den mentalen Aktivitäten im Store hinzugefügt wurde

    useEffect(() => {
      getStorageMentalExercises()

    }, [storeMentalExercises])

        //lädt Aktivitäten aller bisherigen Tage und rendert ProgressScreen, sobald eine neue Aktivität den sozialen Aktivitäten im Store hinzugefügt wurde

    useEffect(() => {
        getStorageSocialExercises()
  
      }, [storeSocialExercises])


          //lädt Aktivitäten aller bisherigen Tage und rendert ProgressScreen, sobald eine neue Aktivität den custom Aktivitäten im Store hinzugefügt wurde

    useEffect(() => {
     
      getStorageCustomExercises()
    }, [storeCustomActivities])


    // useEffect(() => {
    //     console.log("ProgressScreen: Store mental exercise state changed.")

    //     let newData = [...storeMentalExercises]
    //     let newData2 = []

    //     newData = newData.map(({title}) => {
    //         newData2.push(title + '\n')
    //     })
        
    //     setMentalExerciseNames(newData2.toString() + '\n')
      
    // }, [storeMentalExercises])


    // useEffect(() => {
    //     console.log("ProgressScreen: Store social exercise state changed.")

    //     let newData = [...storeSocialExercises]
    //     let newData2 = []

    //     newData = newData.map(({title}) => {
    //         newData2.push(title + '\n')
    //     })
        
    //     setSocialExerciseNames(newData2.toString() + '\n')
      
    // }, [storeSocialExercises])


    // useEffect(() => {
    //     console.log("ProgressScreen: Store custom activities state changed.")

    //     let newData = [...storeCustomActivities]
    //     let newData2 = []

    //     newData = newData.map(({title}) => {
    //         newData2.push(title + '\n')
    //     })
        
    //     setCustomActivitiesNames(newData2.toString() + '\n')
      
    // }, [storeCustomActivities])



    useEffect(() => {
        console.log("ProgressScreen: First render")
        getStorageMood()
    }, [])


    return (
        <SafeAreaView style={styles.screenView} >
        
            <Timeline
                data={data}
                style={styles.timelineView}
                timeStyle={styles.timeView}
                titleStyle={{bottom: 11}}
                // eventContainerStyle={{height: 150}}
                innerCircle='dot'
                separator={true}
                lineWidth={3}
                lineColor='#e28282' 
                circleSize={20}
                circleColor='#e28282'
            />

        </SafeAreaView>   
    );
}

const styles = StyleSheet.create({ 
    screenView: {
        flex: 1,
        justifyContent: 'center',
        alignContent: 'center',
        paddingTop: Platform.OS === "android" ? StatusBar.currentHeight : 0

    },

    gradientBackground: {
        top: -12,
        left: -18,
        // width: 388,
        // height: 664,
        width: '200%',
        height: '200%',
        backgroundColor: "transparent",
        position: "absolute",
    },

    mainContentView: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
    },

    timelineView: {
        paddingLeft: 20,
        paddingRight: 20,

    },

    timeView: {
        backgroundColor: '#e28282',
        color: 'white',
        fontWeight:'bold',
        fontSize: 16,
        width: 80,
        height: 20,
        borderRadius: 10,
        paddingRight: 10
    }

})

export default ProgressScreen;