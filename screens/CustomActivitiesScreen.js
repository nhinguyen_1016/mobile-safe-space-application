import React, { useEffect, useState } from 'react';
import { View, Text, SafeAreaView, StatusBar, Platform, FlatList, TouchableHighlight } from 'react-native';

import { StyleSheet } from 'react-native';
import { LinearGradient } from "expo-linear-gradient";

import ExerciseDetailWidget from '../components/ExerciseDetailWidget';
import ExerciseDetailScreen from './ExerciseDetailScreen';

import {store} from '../redux/store';
import { useSelector } from 'react-redux';

import AsyncStorage from '@react-native-async-storage/async-storage';
import asyncStorageKeys from '../data/asyncStorageKeys';

function CustomActivitiesScreen({ navigation, route}) {

    const storeDoneActivities = useSelector(state => state.activityReducer.doneActivities)
    const storeData = useSelector(state => state.activityReducer.activities)
    const [data, setData] = useState(storeData)

    const [exerciseStorageData, setExerciseStorageData] = useState('')


    const defineDate = async() => {
        const currentDate = new Date().toLocaleDateString()

        if(JSON.stringify(currentDate) == JSON.stringify('24.8.2023')){
            setExerciseStorageData(asyncStorageKeys.customExercises_day1_storage)

            
        }else if(JSON.stringify(currentDate) == JSON.stringify('25.8.2023')){
            setExerciseStorageData(asyncStorageKeys.customExercises_day1_storage)

        }else if(JSON.stringify(currentDate) == JSON.stringify('26.8.2023')){
           setExerciseStorageData(asyncStorageKeys.customExercises_day2_storage)

        }else if(JSON.stringify(currentDate) == JSON.stringify('27.8.2023')){
            setExerciseStorageData(asyncStorageKeys.customExercises_day3_storage)
 
        }else if(JSON.stringify(currentDate) == JSON.stringify('28.8.2023')){
            setExerciseStorageData(asyncStorageKeys.customExercises_day4_storage)
 
        }else if(JSON.stringify(currentDate) == JSON.stringify('29.8.2023')){
            setExerciseStorageData(asyncStorageKeys.customExercises_day5_storage)
 
            //für Testzwecke außerhalb des Playtestings:
        } else{
            setExerciseStorageData(asyncStorageKeys.customExercises_day1_storage)

        }
    }

    // Speichern der gemachten custom activities in den AsyncStorage (für das Rendern in der Timeline)
    const storeDataToStorage = async () => {
        defineDate()

        let exercise
        let exerciseToStorage = []

        storeDoneActivities.map((currentExercise) => {
            exercise = {
                "day" : new Date().toLocaleDateString('en-US'),
                "customExercises" : currentExercise.title,
                "done": true
                
            }
            exerciseToStorage.push(exercise)
        })

        try{
               
            const data = await AsyncStorage.setItem(JSON.stringify(exerciseStorageData), JSON.stringify(exerciseToStorage))

        }catch(e){
            console.log(e)
        }
    }

    useEffect(() => {
        storeDataToStorage()
    }, [storeDoneActivities])

    useEffect(() => {

        console.log("CustomActivitiesScreen: category title: " + route.params.title)
        console.log("CustomActivitiesScreen: unfiltered activities: " + JSON.stringify(storeData))


        let newData = [...data]
        newData = newData.filter(activity => activity.category == route.params.title)
            .map(({title, category, description, index, id, done, key}) => ({title, category, description, index, id, done, key }))

        setData(newData)

        console.log("CustomActivitiesScreen: filtered activities by category " + route.params.title + ": " + JSON.stringify(newData))

    }, [route.params])

    return (
        <SafeAreaView style={styles.screenView}>

            <LinearGradient
                style={styles.gradientBackground}
                locations={[0, 0.99, 0.99]}
                colors={["#e28282", "rgba(243, 162, 57, 0.42)", "rgba(255, 185, 4, 0)"]}
            />

            <View style={styles.mainContentView}>


                <FlatList
                    extraData={[storeDoneActivities, storeData]}
                    contentContainerStyle={styles.flatListView}
                    data={data}
                    keyExtractor={item => item.key}
                    showsHorizontalScrollIndicator={false}
                    renderItem={({ item, index }) => {

                        //Vergleich zwischen Custom Activities in dem Store und State
                        //bei identischer Aktivität -> wurde schon abgeschlossen -> Deaktivieren des Abgeschlossen-Buttons
                        //wird zu Beginn eines neuen Tages wieder zurückgesetzt (in Homescreen)
                        let currentData;
                        if(storeDoneActivities.length !== 0 ){
                            console.log("CustomActivitiesScreen: \n storeDoneActivities array is not empty. ")
                            currentData = storeDoneActivities.find(exercise2 => exercise2.id === item.id)
                                if(currentData) {
                                    console.log("Match found! " + JSON.stringify(currentData))
                                }else{
                                    console.log("No match found!")
                                }
                        }else{
                            console.log("CustomActivitiesScreen: \n StoreDoneActivities array still empty so far.")
                        } 

                         return(
                            <ExerciseDetailWidget
                                key = {item.key}
                                index = {item.index}
                                id = {item.id}
                                title = {item.title} 
                                description={item.description}
                                stackName = {'exercise_detail_screen'}
                                done={!currentData ? item.done : currentData.done}
                                category={item.category}
                                />
                        )
                    }}
                />  

                <TouchableHighlight 
                    activeOpacity={0.5}
                    underlayColor={'black'}
                    onPressIn={() => navigation.navigate('create_activity_screen', {
                        type: 'CustomActivity2',
                        category: route.params.title
                    })}
                >
                    <View style = {styles.addCategoryButton}>
                        <Text style= {styles.buttonText}>
                            +
                        </Text>
                    </View>
                    
                </TouchableHighlight>        

            </View>
        </SafeAreaView>
    );
}

const styles = StyleSheet.create({

    screenView: {
        flex: 1,
        justifyContent: 'center',
        alignContent: 'center',
        paddingTop: Platform.OS === "android" ? StatusBar.currentHeight : 0
    
    },

    gradientBackground: {
        top: -12,
        left: -18,
        // width: 388,
        // height: 664,
        width: '200%',
        height: '200%',
        backgroundColor: "transparent",
        position: "absolute",
    },

    mainContentView: {
        flex: 1,
        justifyContent: 'flex-start',
        alignItems: 'center',
    },

    flatListView: {
        justifyContent: 'flex-start',
        marginTop: 10,
        paddingBottom: 40,
        gap: 50
    },

    addCategoryButton: {
        backgroundColor: 'white',
        borderRadius: 50,
        width: 50,
        height: 50,
        justifyContent: 'center',
        alignItems: 'center',
        position: 'absolute',
        bottom: 20,
        left: 110,
        shadowColor: 'black',
        shadowOpacity: 1,
        elevation: 4,
        shadowRadius: 4,
        shadowOffset: {
            width: 0,
            height: 4
        },
        
    },

    buttonText: {
        fontSize: 20,
        fontWeight: 'bold',
    }

})

export default CustomActivitiesScreen;