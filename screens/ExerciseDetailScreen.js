import React, {useEffect, useState} from 'react';
import { View, Text, SafeAreaView, FlatList, Button } from 'react-native';
import { useNavigation } from '@react-navigation/native';

import { StyleSheet } from 'react-native';

import DetailImage from '../components/DetailImage';
import DetailDescription from '../components/DetailDescription';

import {HeaderBackButton} from '@react-navigation/elements'

function ExerciseDetailScreen({route}) {
   
    const navigation = useNavigation();

    const [mainContent, setMainContent] = useState([
        { key: '0', name: 'ExercisePic'},
        { key: '1', name: 'Main Content' },
       
    ])


    const {id, title, description, backToActivityScreen} = route.params
    var type;
    var number;

    if(id.includes('physical')){
        type = 'physical'

    } else if(id.includes('mental')){
        type = 'mental'

    } else if(id.includes('social')){
        type = 'social'
    
    } else if(id.includes('CustomActivity_')){
        type = "customActivity2"
    }

    if(id.endsWith('1')){
        number = '1'

    }

    //Überprüfung, von welchem Screen aus zu ExerciseDetailScreen navigiert wurde, um wieder in der Navigation zurückzugehen
    useEffect(() => {
        if(route.params.backToActivityScreen == true){
            console.log("ExerciseDetailScreen: Going back to category-detail screen from here on out.")
           
            navigation.setOptions({
                headerShown: true,
                headerLeft: () => {
                    <HeaderBackButton
                        onPress={() => navigation.navigate('category_detail_screen')}
                    />
                }
            })

        }else{
            console.log("ExerciseDetailScreen: NOT going back to category-detail screen.")
        }
    // }, [route.params.backToActivityScreen])
}, [navigation])


    return (
        
        <SafeAreaView style={styles.screenView}>
            <View style={styles.mainContentView}>

                <FlatList
                    data={mainContent}
                    keyExtractor={item => item.key}
                    showsHorizontalScrollIndicator={false}
                    renderItem={({ item, index }) => {
                        if (index === 0)
                            return <DetailImage 
                                type={type} 
                               
                            />
                            
                        if (index === 1)
                            return (
                                <DetailDescription
                                    title={title}
                                    description={description}
                                    type={type}
                                />
                            )
                    }}
                />

            </View>
        </SafeAreaView>
    );
}


const styles = StyleSheet.create({

    screenView: {
        flex: 1,
        justifyContent: 'center',
        alignContent: 'center',
    },

    mainContentView: {
        flex: 1,
        justifyContent: 'flex-start',
        alignItems: 'center',
    },

    dailyThemePic: {
        width: 360,
        // width: '200%',
        height: 250,
            
    },
   
})

export default ExerciseDetailScreen;