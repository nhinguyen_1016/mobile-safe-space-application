import React, { useEffect, useState, useRef } from 'react';
import { View, Text, SafeAreaView, StatusBar, Platform } from 'react-native';

import { StyleSheet } from 'react-native';
import { LinearGradient } from "expo-linear-gradient";

import ExerciseDetailWidget from '../components/ExerciseDetailWidget';
import ExerciseDetailScreen from './ExerciseDetailScreen';

import { useSelector, useDispatch } from 'react-redux';
import { store } from '../redux/store';

import AsyncStorage from '@react-native-async-storage/async-storage';
import asyncStorageKeys from '../data/asyncStorageKeys';

import * as Notifications from 'expo-notifications'
import NotificationPopup from 'react-native-push-notification-popup'

Notifications.setNotificationHandler({
  handleNotification: async () => ({
    shouldShowAlert: true,
    shouldPlaySound: false,
    shouldSetBadge: false,
  })
})


function MentalExerciseScreen({ navigation }) {

    const notificationData = require('../data/localNotifications.json');
    const [notification, setNotification] = useState(false)
    const notificationListener = useRef()
    const responseListener = useRef()

    const mentalExerciseData = require('../data/dailyMentalExercise.json');
    const storeMentalActivity = store.getState().exerciseReducer.mentalExercises
    const storeMentalActivity2 =useSelector(state => state.exerciseReducer.mentalExercises)
    const storeBadges = store.getState().badgeReducer.badges
    
    const [exercises, setExercises] = useState([])
    const [exerciseStorageData, setExerciseStorageData] = useState('')


    const renderDailyExercise = async () => {

        const exercise = {
            key: '', 
            index: '', 
            id: '',
            title: '', 
            description: '', 
            stackName: 'exercise_detail_screen',
            done: false
        }


        const currentDate = new Date().toLocaleDateString()

        // switch(JSON.stringify(currentDate)){
            switch(JSON.stringify('25.8.2023')){

           
            case JSON.stringify('25.8.2023'):
                exercise.key = mentalExerciseData.mentalDay1.mentalExercise1_1.key
                exercise.index = mentalExerciseData.mentalDay1.mentalExercise1_1.key
                exercise.id = mentalExerciseData.mentalDay1.mentalExercise1_1.id,
                exercise.title = mentalExerciseData.mentalDay1.mentalExercise1_1.title,
                exercise.description = mentalExerciseData.mentalDay1.mentalExercise1_1.description
                  break;

            case JSON.stringify('26.8.2023'):
                exercise.key = mentalExerciseData.mentalDay2.mentalExercise2_1.key
                exercise.index = mentalExerciseData.mentalDay2.mentalExercise2_1.key
                exercise.id = mentalExerciseData.mentalDay2.mentalExercise2_1.id,
                exercise.title = mentalExerciseData.mentalDay2.mentalExercise2_1.title,
                exercise.description = mentalExerciseData.mentalDay2.mentalExercise2_1.description
                
                break;


            case JSON.stringify('27.8.2023'):
                exercise.key = mentalExerciseData.mentalDay3.mentalExercise3_1.key
                exercise.index = mentalExerciseData.mentalDay3.mentalExercise3_1.key
                exercise.id = mentalExerciseData.mentalDay3.mentalExercise3_1.id,
                exercise.title = mentalExerciseData.mentalDay3.mentalExercise3_1.title,
                exercise.description = mentalExerciseData.mentalDay3.mentalExercise3_1.description

                
                 break;


            case JSON.stringify('28.8.2023'):
                exercise.key = mentalExerciseData.mentalDay4.mentalExercise4_1.key
                exercise.index = mentalExerciseData.mentalDay4.mentalExercise4_1.key
                exercise.id = mentalExerciseData.mentalDay4.mentalExercise4_1.id,
                exercise.title = mentalExerciseData.mentalDay4.mentalExercise4_1.title,
                exercise.description = mentalExerciseData.mentalDay4.mentalExercise4_1.description

             
                 break;


            case JSON.stringify('29.8.2023'):
                exercise.key = mentalExerciseData.mentalDay5.mentalExercise5_1.key
                exercise.index = mentalExerciseData.mentalDay5.mentalExercise5_1.key
                exercise.id = mentalExerciseData.mentalDay5.mentalExercise5_1.id,
                exercise.title = mentalExerciseData.mentalDay5.mentalExercise5_1.title,
                exercise.description = mentalExerciseData.mentalDay5.mentalExercise5_1.description

              
                 break;

            default: 
            exercise.key = mentalExerciseData.mentalDay1.mentalExercise1_1.key
            exercise.index = mentalExerciseData.mentalDay1.mentalExercise1_1.key
            exercise.id = mentalExerciseData.mentalDay1.mentalExercise1_1.id,
            exercise.title = mentalExerciseData.mentalDay1.mentalExercise1_1.title,
            exercise.description = mentalExerciseData.mentalDay1.mentalExercise1_1.description
              break;

         }  
       
        let newMainContent = [...exercises]
        let data = {...newMainContent[0]}
        data = exercise
        newMainContent[0] = data


        setExercises(newMainContent)
        
    }

    const defineDate = async() => {
        const currentDate = new Date().toLocaleDateString()

        if(JSON.stringify(currentDate) == JSON.stringify('24.8.2023')){
            setExerciseStorageData(asyncStorageKeys.mentalExercises_day1_storage)

            
        }else if(JSON.stringify(currentDate) == JSON.stringify('25.8.2023')){
            setExerciseStorageData(asyncStorageKeys.mentalExercises_day1_storage)

        }else if(JSON.stringify(currentDate) == JSON.stringify('26.8.2023')){
           setExerciseStorageData(asyncStorageKeys.mentalExercises_day2_storage)


        }else if(JSON.stringify(currentDate) == JSON.stringify('27.8.2023')){
            setExerciseStorageData(asyncStorageKeys.mentalExercises_day3_storage)
 
        }else if(JSON.stringify(currentDate) == JSON.stringify('28.8.2023')){
            setExerciseStorageData(asyncStorageKeys.mentalExercises_day4_storage)
 
        }else if(JSON.stringify(currentDate) == JSON.stringify('29.8.2023')){
            setExerciseStorageData(asyncStorageKeys.mentalExercises_day5_storage)
 
        } else{
            setExerciseStorageData(asyncStorageKeys.mentalExercises_day1_storage)
 
        } 
    }

    const storeDataToStorage = async () => {
        defineDate()

        let exercise
        let exerciseToStorage = []

        storeMentalActivity2.map((currentExercise) => {
            exercise = {
                "day" : new Date().toLocaleDateString(),
                "mentalExercises" : currentExercise.title,
                "done" : true
                
            }
            exerciseToStorage.push(exercise)
        })

        try{
            const data = await AsyncStorage.setItem(JSON.stringify(exerciseStorageData), JSON.stringify(exerciseToStorage))

        }catch(e){
            console.log(e)
        }

        
        try{
            const data2 = await AsyncStorage.getItem(JSON.stringify(exerciseStorageData))
           JSON.parse(data2)
           console.log("MentalExerciseScreen: Successfully got mental exercises to AsyncStorage! \n Data is: " + data2 )

       }catch(e){
           console.log(e)
       }
    }


    const schedulePushNotification = async () =>  {
        await Notifications.scheduleNotificationAsync({
          content: {
            title: notificationData.MH_1_badge.title,
            body: notificationData.MH_1_badge.body
          },
          trigger: {seconds: 1},
        })
    
        popup.show({
          onPress: function() {
            navigation.navigate('badges_screen')
          },
          appTitle: 'Safe Space Anwendung',
          timeText: '',
          title: notificationData.MH_1_badge.title,
          body: notificationData.MH_1_badge.body,
          slideOutTime: 4000,
        })
      }

        const renderCustomPopup = ({ appIconSource, appTitle, timeText, title, body }) => (
        <View style= {styles.localNotification}>
            <Text style={styles.localNotificationText}>
                {title}
            </Text>
            <Text style={[styles.localNotificationText, {fontWeight: 'normal'}]}>
                {body}
            </Text>
        </View>
    );


    useEffect(() => {
        storeDataToStorage()


        let badge = storeBadges.find((badge) => badge.key == '0')
        if(!badge && store.getState().exerciseReducer.mentalExercisePoints == 4){
            schedulePushNotification()
        }else{

        }
        


    }, [storeMentalActivity2])

    useEffect(() => {

        renderDailyExercise()

        notificationListener.current = Notifications.addNotificationReceivedListener(notification => {
            console.log("NOTIFICAITON RECEIVED")
            setNotification(notification)
        })
                
        responseListener.current = Notifications.addNotificationResponseReceivedListener(response => {
            console.log(response)
            navigation.navigate('badges_screen')
        })

        return () => {
            Notifications.removeNotificationSubscription(notificationListener.current)
            Notifications.removeNotificationSubscription(responseListener.current)
        }

    }, [])

    

    return (
        <SafeAreaView style={styles.screenView}>

            <LinearGradient
                style={styles.gradientBackground}
                locations={[0, 0.99, 0.99]}
                colors={["#e28282", "rgba(243, 162, 57, 0.42)", "rgba(255, 185, 4, 0)"]}
            />

            <View style={styles.mainContentView}>

                {exercises.map(exercise => {
                     let currentData;
                     if(storeMentalActivity.length !== 0 ){
                         console.log("MentalExerciseScreen: \n StoreActivity array is not empty. ")
                         currentData = storeMentalActivity.find(exercise2 => exercise2.id === exercise.id)
                             if(currentData) {
                                 console.log("Match found! " + JSON.stringify(currentData))
                             }else{
                                 console.log("No match found!")
                             }
                     }else{
                         console.log("MentalExerciseScreen: \n StoreActivity array still empty so far.")
                     }   

                    return(
                        <ExerciseDetailWidget
                            key = {exercise.index}
                            index = {exercise.index}
                            id = {exercise.id}
                            title = {exercise.title} 
                            description={exercise.description}
                            stackName = {exercise.stackName}
                            done={!currentData ? exercise.done : currentData.done}
                        />
                    )
                })}

            </View>

            <NotificationPopup
                    ref={ref => popup = ref}
                    renderPopupContent={renderCustomPopup}
                    shouldChildHandleResponderStart={true}
                    shouldChildHandleResponderMove={true}
                    isSkipStatusBarPadding={true}
            />

        </SafeAreaView>
    );
}

const styles = StyleSheet.create({

    screenView: {
        flex: 1,
        justifyContent: 'center',
        alignContent: 'center',
        paddingTop: Platform.OS === "android" ? StatusBar.currentHeight : 0
    
    },

    gradientBackground: {
        top: -12,
        left: -18,
        // width: 388,
        // height: 664,
        width: '200%',
        height: '200%',
        backgroundColor: "transparent",
        position: "absolute",
    },

    mainContentView: {
        flex: 1,
        justifyContent: 'flex-start',
        alignItems: 'center',
    },

    localNotification: {
        backgroundColor: 'white',
        borderRadius: 5,
        width: 300,
        height: 40,
        justifyContent: 'flex-start',
        alignContent: 'flex-start'
        
    },

    localNotificationText: {
        fontSize: 14,
        fontWeight: 'bold',
        paddingLeft: '5%'
    }

})

export default MentalExerciseScreen;