import React, {useEffect, useState} from 'react';
import { View, Text, SafeAreaView, FlatList } from 'react-native';
import { useNavigation } from '@react-navigation/native';

import { StyleSheet } from 'react-native';
import { LinearGradient } from "expo-linear-gradient";

import DetailImage from '../components/DetailImage';
import DetailDescription from '../components/DetailDescription';

function EditCustomActivityScreen({route}) {

    const [mainContent, setMainContent] = useState([
        { key: '0', name: 'ExercisePic', type: 'customActivity'},
        { key: '1', name: 'Main Content', type: 'customActivity', editable: true },
    ])

    const [editTitle, setEditTitle] = useState('')
    const [editDescription, setEditDescription] = useState('')


    useEffect(() => {
        if(!route.params){

        }else{
            console.log("EditCustomActivityScreen: Route params for EDITING activity defined like following: " + JSON.stringify(route.params))
        }

    },[])

    return (
        
        <SafeAreaView style={styles.screenView}>
            <View style={styles.mainContentView}>

                <FlatList
                    data={mainContent}
                    keyExtractor={item => item.key}
                    showsHorizontalScrollIndicator={false}
                    renderItem={({ item, index }) => {
                        if (index === 0)
                            return <DetailImage 
                                type={item.type}
                            />
                      
                        if (index === 1)
                            return (
                                <DetailDescription
                                    type={item.type}
                                    editable={item.editable}
                                    title={route.params.name}
                                    description={route.params.content}
                                    index={route.params.index}

                                />
                            )
                            
                    }
                }
                />

            </View>
        </SafeAreaView>
    );
}


const styles = StyleSheet.create({

    screenView: {
        flex: 1,
        justifyContent: 'center',
        alignContent: 'center',
    },

    gradientBackground: {
        top: -12,
        left: -18,
        // width: 388,
        // height: 664,
        width: '200%',
        height: '200%',
        backgroundColor: "transparent",
        position: "absolute",
    },

    mainContentView: {
        flex: 1,
        justifyContent: 'flex-start',
        alignItems: 'center',
    },

    dailyThemePic: {
        width: 360,
        // width: '200%',
        height: 250,
            
    },
   
})

export default EditCustomActivityScreen;