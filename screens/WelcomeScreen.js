import React from 'react';
// import { useNavigation } from "@react-navigation/native";
import { View, Text, TextInput, Pressable, Image, SafeAreaView, Platform, StatusBar } from 'react-native';

import { StyleSheet } from 'react-native';
import { LinearGradient } from "expo-linear-gradient";


//TO DO
//save name of user in state/local or asyncstorage
//if name is known -> skip this screen, straight to home

function WelcomeScreen() {

    // const navigation = useNavigation();


    return (
        <SafeAreaView style={styles.screenView}>
            <LinearGradient
                style={styles.gradientBackground}
                locations={[0, 0.99, 0.99]}
                colors={["#e28282", "rgba(243, 162, 57, 0.42)", "rgba(255, 185, 4, 0)"]}
            />

            <View style={styles.mainContentView} >
                <View style={styles.whiteTextView}>
                    <Text style={styles.text} >Herzlich willkommen. {"\n"}Bitte gebe deinen Namen ein.</Text>
                </View>
                <TextInput style={styles.nameInputField} textAlign='center' text></TextInput>
                <Pressable
                    style={styles.submitName}
                //   onPress={() => navigation.navigate("HomeScreen")}
                >
                    <Image
                        style={styles.icon}
                        contentFit="cover"
                        source={require("../assets/button-to-home-screen.png")}
                    />
                </Pressable>
            </View>
        </SafeAreaView>
    );
}

const styles = StyleSheet.create({

    screenView: {
        flex: 1,
        justifyContent: 'center',
        alignContent: 'center',
        paddingTop: Platform.OS === "android" ? StatusBar.currentHeight : 0

    },

    gradientBackground: {
        top: -12,
        left: -18,
        width: 388,
        height: 664,
        backgroundColor: "transparent",
        position: "absolute",
    },

    mainContentView: {
        justifyContent: 'center',
        alignItems: 'center',
    },

    whiteTextView: {
        backgroundColor: 'white',
        borderRadius: 10,
        width: 300,
        height: 120,
        alignItems: "center",
        justifyContent: "center",
        shadowColor: 'black',
        shadowOpacity: 1,
        elevation: 4,
        shadowRadius: 4,
        shadowOffset: {
            width: 0,
            height: 4
        },
    },

    text: {
        fontSize: 20,
        fontFamily: 'Montserrat',
        color: 'black'
    },

    nameInputField: {
        backgroundColor: 'white',
        borderRadius: 10,
        width: 250,
        height: 40,
        marginTop: 45,
        fontSize: 20,
        fontFamily: 'Montserrat',
        shadowColor: 'black',
        shadowOpacity: 1,
        elevation: 4,
        shadowRadius: 4,
        shadowOffset: {
            width: 0,
            height: 4
        },

    },

    submitName: {
        width: 25,
        height: 36,
        marginTop: 45,
    },

    icon: {
        height: "100%",
        width: "100%",

    }

})

export default WelcomeScreen;