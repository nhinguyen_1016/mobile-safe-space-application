import TabNavigator from './components/TabNavigator';
import {Provider} from 'react-redux'
import { store } from './redux/store';
import {PersistGate} from 'redux-persist/integration/react'
import {persistStore} from 'redux-persist'
import { Pressable, Text } from 'react-native';

import AsyncStorage from '@react-native-async-storage/async-storage';


let persistor = persistStore(store)


//Einstiegspunkt der Anwendung
function App(){

  //für Testzwecke, um persistent Daten aus dem lokalen Speicher und Store wieder zu löschen
  const handleResetPersistedStore = () => {
    console.log("App: handleResetPersistedStore()")
    persistor.pause()
    persistor.flush().then(() => {
      return persistor.purge()
    })
    console.log("App: handleResetPersistedStore() done.")
  }

  const handleResetAsyncStorage = async () => {
    console.log("App: handleResetAsyncStorage()")
    try{
          await AsyncStorage.clear()
          console.log("App: handleResetAsyncStorage() done.")
    }catch(e){
      console.log(e)
    }
  }


    //Rendern des TabNavigators, der daraufhin den HomeNavigator mitsamt des LoadingScreens aufruft
    return (
    <Provider store={store}>
      <PersistGate persistor={persistor} >

      {/* für Testzwecke -> AUSKOMMENTIEREN, um persistent Daten aus dem lokalen Speicher und Store wieder zu löschen */}
         
         {/* <Pressable onPress={handleResetPersistedStore} style={{height: '10%', justifyContent: 'center', alignContent: 'center', left: '25%'}} >
          <Text>
            RESET PERSISTED STORE
          </Text>
        </Pressable>
        
         <Pressable onPress={handleResetAsyncStorage} style={{height: '10%', justifyContent: 'center', alignContent: 'center', left: '25%'}} >
          <Text>
            RESET ASYNC STORAGE
          </Text>
        </Pressable>  */}
        
        <TabNavigator/>
      </PersistGate>
    </Provider>
  );
}

export default App;

